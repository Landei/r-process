package rprocess

import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

object Settings {

    private const val fileName = "game/game.properties"
    private val properties = Properties().apply {
        try {
            load(FileInputStream(fileName))
        } catch (e: IOException) {
            /* ignore */
        }
    }
    private lateinit var bundle: ResourceBundle

    var width: Int
        get() = properties.getProperty("width", "1500").toInt()
        set(value) {
            properties.setProperty("width", value.toString())
        }

    var height: Int
        get() = properties.getProperty("height", "1000").toInt()
        set(value) {
            properties.setProperty("height", value.toString())
        }

    var locale: Locale
        get() = Locale.forLanguageTag(properties.getProperty("locale", "en"))
        set(value) {
            properties.setProperty("locale", value.toLanguageTag())
            bundle = ResourceBundle.getBundle("translations/messages", locale)
        }

    fun save() {
        properties.store(FileOutputStream(fileName), null)
    }

    fun i18n(key: String): String = bundle.getString(key)
}