package rprocess.generators

import rprocess.model.Pos
import rprocess.model.galaxy.Planet
import rprocess.model.galaxy.PlanetType
import rprocess.model.galaxy.Star
import rprocess.model.resources.Deposit
import rprocess.model.resources.Resource
import rprocess.util.random
import java.util.*
import io.vavr.collection.HashMap as VHashMap
import io.vavr.collection.List as VList

object PlanetG {

    fun generateFor(star: Star, rng: Random) : Star {
        val planets = VList.of(
                generateFor(star, "a", 300F, PlanetType.values().random(rng) , rng),
                generateFor(star, "b", 700F, PlanetType.values().random(rng), rng),
                generateFor(star, "c", 1100F, PlanetType.values().random(rng), rng)
        )
        return star.copy(planets = planets)
    }

    private fun generateFor(star: Star, suffix: String, distance: Float, planetType: PlanetType, rng: Random): Planet {
        val radius = 30F + rng.nextInt(15)
        val deposits = VHashMap.of(
                Pos(0,0), Deposit( Resource.HYDROGEN, 5),
                Pos(0,1), Deposit( Resource.OXYGEN, 20),
                Pos(1,1), Deposit( Resource.HELIUM, 2),
                Pos(0,2), Deposit( Resource.WATER, 100),
                Pos(0,3), Deposit(Resource.QUARTZ, 100),
                Pos(1,3), Deposit(Resource.URANIUM, 5))
        val planet = Planet("${star.name} $suffix", radius, deposits, distance, planetType, VList.empty())
        return MoonG.generateFor(planet, rng)
    }
}