package rprocess.generators

import rprocess.model.Pos
import rprocess.model.galaxy.Star
import rprocess.model.galaxy.StarType
import rprocess.model.resources.Deposit
import rprocess.model.resources.Resource
import rprocess.util.random
import java.util.*
import io.vavr.collection.HashMap as VHashMap
import io.vavr.collection.List as VList

class StarG(private val rng: Random) {

    private val names = StarG::class.java.getResource("/starNames.txt")
        .readText().lines().toList().shuffled(rng)
    private var nameIndex = 0

    fun generate(): Star {
        val deposits = VHashMap.of(
                Pos(0,0), Deposit(Resource.HYDROGEN, 100),
                Pos(0,1), Deposit(Resource.HYDROGEN, 50),
                Pos(1,1), Deposit(Resource.HELIUM, 50),
                Pos(0,2), Deposit(Resource.HELIUM, 100))
        val starType = StarType.values().random(rng)
        val star = Star(names[nameIndex++], 100F, deposits, starType, VList.empty())
        return PlanetG.generateFor(star, rng)
    }
}