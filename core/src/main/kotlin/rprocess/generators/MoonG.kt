package rprocess.generators

import rprocess.model.Pos
import rprocess.model.galaxy.Moon
import rprocess.model.galaxy.MoonType
import rprocess.model.galaxy.Planet
import rprocess.model.resources.Deposit
import rprocess.model.resources.Resource
import rprocess.util.random
import kotlin.random.Random
import io.vavr.collection.HashMap as VHashMap
import io.vavr.collection.List as VList

object MoonG {

    fun generateFor(planet: Planet, rng: java.util.Random): Planet {
        val deposits = VHashMap.of(
                Pos(0, 1), Deposit(Resource.HYDROGEN, 20),
                Pos(0, 2), Deposit(Resource.WATER, 10),
                Pos(1, 2), Deposit(Resource.HELIUM, 2),
                Pos(0, 3), Deposit(Resource.QUARTZ, 20))

        var moons = VList.empty<Moon>()
        if (Random.nextBoolean()) {
            moons = moons.append(Moon(planet.name + " I", 10F, deposits, planet.radius + 25F, MoonType.values().random(rng)))
            if (Random.nextBoolean()) {
                moons = moons.append(Moon(planet.name + " II", 15F, deposits, planet.radius + 60F, MoonType.values().random(rng)))
            }
        }
        return planet.copy(moons = moons)
    }
}