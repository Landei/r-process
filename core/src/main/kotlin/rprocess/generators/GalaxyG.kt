package rprocess.generators

import io.vavr.Tuple
import io.vavr.kotlin.pair
import rprocess.model.galaxy.Coord
import rprocess.model.galaxy.Galaxy
import rprocess.model.galaxy.Route
import java.util.*
import kotlin.math.max
import kotlin.math.min
import io.vavr.collection.HashSet as VHashSet
import io.vavr.collection.List as VList
import io.vavr.collection.Set as VSet


object GalaxyG {

    fun generate(rng: Random): Galaxy {

        val coords: VList<Coord> = VList.ofAll((-35..30).flatMap { x ->
            (-20..18).map { y ->
                Coord(x, y)
            }
        }.shuffled(rng)).take(600)
                .foldLeft(VList.of()) { accu, coord ->
                    when {
                        accu.any { it.dist(coord) < 4 } -> accu
                        else -> accu.push(coord)
                    }
                }

        val starG = StarG(rng)
        val systems = coords.toMap { coord -> Tuple.of(coord, starG.generate()) }

        val routes = makeRoutes(coords)

        return Galaxy(systems, routes)
    }

    fun makeRoutes(coords: VList<Coord>): VSet<Route> {

        val routes = coords.flatMap { c1 ->
            coords.filter { c2 -> c1 < c2 && c1.dist(c2) <= 9 }
                    .map { c2 -> Route(c1, c2) }
        }.toSet().addAll(kruskalMST(coords))

        val crossingLongerRoutes = routes.flatMap { r1 ->
            routes.filter { r2 -> !r1.connected(r2) && r1 crosses r2 }
                    .map { r2 -> if (r1.dist() > r2.dist()) r1 else r2 }
        }
        return routes.removeAll(crossingLongerRoutes)
    }

    fun kruskalMST(coords: VList<Coord>): VSet<Route> {
        var routes = VHashSet.empty<Route>()
        var sets = coords.map { c -> VHashSet.of(c) }
        val dists = coords.flatMap { c1 ->
            coords.filter { c2 -> c1 < c2 }
                    .map { c2 -> Tuple.of(c1, c2) }
        }.sortBy { t -> t._1.dist(t._2) }

        for (tuple in dists) {
            if (sets.size() == 1) {
                return routes
            }
            val (c1, c2) = tuple.pair()
            val i1 = sets.indexWhere { set -> set.contains(c1) }
            val i2 = sets.indexWhere { set -> set.contains(c2) }
            if (i1 != i2) {
                val set1 = sets.get(i1)
                val set2 = sets.get(i2)
                sets = sets.removeAt(max(i1, i2)).removeAt(min(i1, i2)).push(set1.addAll(set2))
                routes = routes.add(Route(c1, c2))
            }
        }
        if (sets.size() == 1) {
            return routes
        }
        error("Kruskal is broken")
    }
}