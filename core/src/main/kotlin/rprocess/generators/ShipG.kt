package rprocess.generators

import io.vavr.Tuple
import io.vavr.kotlin.toVavrList
import rprocess.model.Pos
import rprocess.model.resources.Immaterial
import rprocess.model.resources.Resource
import rprocess.model.ship.*
import java.util.*
import io.vavr.collection.HashMap as VHashMap
import io.vavr.collection.HashSet as VHashSet

object ShipG {

    fun generate(rng: Random): Ship {
        val cells = VHashMap.empty<Pos, Cell>()
                .put(Pos(0, 0), Cargo(Resource.SICK_HUMAN, 1))
                .put(Pos(0, 1), Cargo(Resource.HUMAN, 1))
                .put(Pos(0, 2), Cargo(Resource.CHEMICAL_REACTOR, 1))
                .put(Pos(0, 3), Cargo(Resource.BROKEN_FUSION_DRIVE, 1))
                .put(Pos(1, 0), Cargo(Resource.HELIUM, 20))
                .put(Pos(1, 1), Cargo(Resource.OXYGEN, 20))
                .put(Pos(1, 2), Cargo(Resource.WATER, 10))
                .put(Pos(1, 3), Cargo(Resource.HYDROGEN, 10))
                .put(Pos(2, 0), Cargo(Resource.ENERGY_CELL, 99))
                .put(Pos(2, 1), Cargo(Resource.CARBOHYDRATES, 15))
                .put(Pos(2, 2), DamagedCell)
                .put(Pos(2, 3), Cargo(Resource.IRON, 10))
                .merge(
                        (0..4).flatMap { x ->
                            (0..3).map { y -> Pos(x, y) }
                        }.toVavrList().toMap { pos -> Tuple.of(pos, EmptyCell as Cell) }
                )
        return Ship(ShipType.FREIGHTER, "Sunrise Beta", cells,
                VHashSet.of(Immaterial.ELECTROLYSIS, Immaterial.FLOWER_STALK))
    }

}