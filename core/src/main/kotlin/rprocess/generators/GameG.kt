package rprocess.generators

import rprocess.model.Game
import rprocess.model.history.History
import java.util.*
import io.vavr.collection.List as VList

object GameG {

    enum class Difficulty {
        EASY,
        MEDIUM,
        HARD
    }

    enum class GalaxySize {
        SMALL,
        MEDIUM,
        BIG
    }


    fun generate(difficulty: Difficulty, galaxySize: GalaxySize, seed: Long) : Game {
        val rng = Random(seed)
        val ship = ShipG.generate(rng)
        val galaxy = GalaxyG.generate(rng)
        val startCoord = galaxy.routes.first().c1
        return Game(ship, galaxy, History(VList.of(startCoord), seed))
    }
}