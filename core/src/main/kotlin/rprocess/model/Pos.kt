package rprocess.model

import io.vavr.collection.Stream.range
import io.vavr.collection.List as VList

data class Pos(val x: Int, val y: Int) : Comparable<Pos> {

    override fun compareTo(other: Pos): Int = when {
        this.x != other.x -> this.x.compareTo(other.x)
        else -> this.y.compareTo(other.y)
    }

    companion object {
        fun inRange(maxX: Int, maxY: Int): VList<Pos> =
                range(0, maxX + 1).flatMap { x ->
                    range(0, maxY + 1).map { y ->
                        Pos(x, y)
                    }
                }.toList()
    }

}