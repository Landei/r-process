package rprocess.model.galaxy

enum class PlanetType {
    ROCKY,
    ICY,
    MAGMA,
    HOT,
    DESERT,
    OCEAN,
    PART_OCEAN,
    GAS_GIANT
}