package rprocess.model.galaxy

import kotlin.math.ceil

data class Route(val c1: Coord, val c2: Coord) {

    init {
        require(c1 != c2) { "endpoints of route must be different" }
    }

    fun dist() = ceil(c1.dist(c2).toDouble()).toInt()

    fun between(coord1: Coord, coord2: Coord) = when {
        c1 == coord1 && c2 == coord2 -> true
        c1 == coord2 && c2 == coord1 -> true
        else -> false
    }

    fun contains(coord: Coord) = coord == c1 || coord == c2

    fun connected(that: Route) =
            that.contains(c1) || that.contains(c2)

    infix fun crosses(that: Route): Boolean {
        val bx = this.c2.x - this.c1.x
        val by = this.c2.y - this.c1.y
        val dx = that.c2.x - that.c1.x
        val dy = that.c2.y - that.c1.y
        val bDotD = bx * dy - by * dx
        if (bx * dy - by * dx == 0) {
            return false
        }
        val cx = that.c1.x - this.c1.x
        val cy = that.c1.y - this.c1.y
        val t = 1.0 * (cx * dy - cy * dx) / bDotD
        if (t < 0 || t > 1) {
            return false
        }
        val u = 1.0 * (cx * by - cy * bx) / bDotD
        return 0 <= u && u <= 1
    }
}