package rprocess.model.galaxy

import rprocess.util.i18n

enum class StarType {
    VIOLET,
    BLUE,
    GREEN,
    YELLOW,
    RED,
    WHITE,
    BROWN;

    fun i18n() = "star_type.${name.toLowerCase()}".i18n()
}
