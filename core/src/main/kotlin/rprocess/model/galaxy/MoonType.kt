package rprocess.model.galaxy

enum class MoonType {
    ROCKY,
    ICY,
    HOT,
    MAGMA
}