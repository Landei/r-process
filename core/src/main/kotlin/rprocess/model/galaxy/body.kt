package rprocess.model.galaxy

import io.vavr.collection.List
import rprocess.model.Pos
import rprocess.model.resources.Deposit
import rprocess.util.Lens
import rprocess.util.listL
import io.vavr.collection.Map as VMap

sealed class Body {
    abstract val name: String
    abstract val radius: Float
    abstract val deposits: VMap<Pos, Deposit>
}

data class Star(override val name: String,
                override val radius: Float,
                override val deposits: VMap<Pos, Deposit>,
                val starType: StarType,
                val planets: List<Planet>) : Body() {

    companion object {
        val depositsL = Lens(Star::deposits) { star, deposits -> star.copy(deposits = deposits) }
        val planetsL = Lens(Star::planets) { star, planets -> star.copy(planets = planets) }
        fun planetByNameL(name: String) = planetsL * listL{ it.name == name }
        fun moonByNameL(name: String) = planetsL *
                listL{ it.moons.any { moon -> moon.name == name }} *
                Planet.moonByNameL(name)
    }
}

data class Planet(override val name: String,
                  override val radius: Float,
                  override val deposits: VMap<Pos, Deposit>,
                  val distance: Float,
                  val planetType: PlanetType,
                  val moons: List<Moon>) : Body() {

    companion object {
        val depositsL = Lens(Planet::deposits) { planet, deposits -> planet.copy(deposits = deposits) }
        val moonsL = Lens(Planet::moons) { planet, moons -> planet.copy(moons = moons) }
        fun moonByNameL(name: String) = moonsL * listL{ it.name == name}
    }
}

data class Moon(override val name: String,
                override val radius: Float,
                override val deposits: VMap<Pos, Deposit>,
                val distance: Float,
                val moonType: MoonType) : Body() {

    companion object {
        val nameL = Lens(Moon::name){ moon, name -> moon.copy(name = name) }
        val depositsL = Lens(Moon::deposits){ moon, deposits -> moon.copy(deposits = deposits) }
    }
}