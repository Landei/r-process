package rprocess.model.galaxy

import io.vavr.control.Option
import rprocess.util.Lens
import io.vavr.collection.HashMap as VHashMap
import io.vavr.collection.Map as VMap
import io.vavr.collection.Set as VSet

data class Galaxy(val systems: VMap<Coord, Star>, val routes: VSet<Route>) {

    fun dist(coord1: Coord, coord2: Coord): Option<Int> {
        var dists = VHashMap.of(coord1, 0)
        var todo = routes

        while (!todo.isEmpty) {
            val r = todo.find { route -> dists.keySet().exists { c -> route.contains(c) } }
            if (r.isEmpty) {
                return dists.get(coord2)
            }
            val route = r.get()
            todo = todo.remove(route)
            val d1 = dists.get(route.c1).getOrElse(Int.MAX_VALUE)
            val d2 = dists.get(route.c2).getOrElse(Int.MAX_VALUE)
            when {
                d1 < d2 - route.dist() -> dists = dists.put(route.c2, d1 + route.dist())
                d2 < d1 - route.dist() -> dists = dists.put(route.c1, d2 + route.dist())
            }
        }
        return dists.get(coord2)
    }

    companion object {
        val systemsL = Lens(Galaxy::systems) { galaxy, systems -> galaxy.copy(systems = systems) }
        val routesL = Lens(Galaxy::routes) { galaxy, routes -> galaxy.copy(routes = routes) }
    }

}