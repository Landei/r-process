package rprocess.model.galaxy

import rprocess.util.sqr
import kotlin.math.sqrt


data class Coord(val x: Int, val y: Int) : Comparable<Coord> {
    override fun compareTo(other: Coord): Int =
            when (x) {
                other.x -> y.compareTo(other.y)
                else -> x.compareTo(other.x)
            }

    fun dist(that: Coord): Float = sqrt(sqr(this.x - that.x).toFloat() + sqr(this.y - that.y).toFloat())
}