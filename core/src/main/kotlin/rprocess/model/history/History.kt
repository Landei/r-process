package rprocess.model.history

import rprocess.model.galaxy.Coord
import rprocess.model.galaxy.Route
import io.vavr.collection.List as VList

data class History(val path : VList<Coord>, val seed: Long) {

    fun visit(coord: Coord) = copy(path = path.push(coord))

    fun coord() = path.peek()

    fun hasUsed(route: Route) = path.zip(path.pop()).any { route.between(it._1, it._2) }

}