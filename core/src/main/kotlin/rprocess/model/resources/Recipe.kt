package rprocess.model.resources

import io.vavr.collection.HashMap.of
import io.vavr.control.Option
import rprocess.model.resources.Immaterial.ELECTROLYSIS
import rprocess.model.resources.Immaterial.STEEL_MAKING
import rprocess.model.resources.Resource.*
import rprocess.util.i18n
import io.vavr.collection.List as VList
import io.vavr.collection.Map as VMap


enum class Recipe(val input: VMap<Collectible, Int>, val output: VMap<Collectible, Int>) {
    MAKE_WATER(of(HYDROGEN, 2, OXYGEN, 1, CHEMICAL_REACTOR, 0),
            of(WATER, 1, ENERGY_CELL, 2)),
    SPLIT_WATER(
            of(WATER, 1, ENERGY_CELL, 3, CHEMICAL_REACTOR, 0, ELECTROLYSIS, 0),
            of(HYDROGEN, 2, OXYGEN, 1)),
    MAKE_CARBOHYDRATES(
            of(CARBON, 1, WATER, 1, ENERGY_CELL, 2, CHEMICAL_REACTOR, 0),
            of(CARBOHYDRATES, 1)),
    MAKE_STEEL(
            of(CARBON, 1, IRON, 2, ENERGY_CELL, 3, CHEMICAL_REACTOR, 0, STEEL_MAKING, 0),
            of(STEEL, 2)),
    SPLIT_URANIUM(
            of(URANIUM, 1, WATER, 5, NUCLEAR_REACTOR, 0),
            of(ENERGY_CELL, 20)),
    MAKE_SILICON(
            of(QUARTZ, 1, ENERGY_CELL, 3, CHEMICAL_REACTOR, 0),
            of(SILICON, 1, OXYGEN, 2)),
    MAKE_IRON(
            of(IRON_ORE, 2, ENERGY_CELL, 2, CHEMICAL_REACTOR, 0),
            of(IRON, 1, OXYGEN, 1)),
    BURN_CARBON(
            of(CARBON, 1, OXYGEN, 2, CHEMICAL_REACTOR, 0),
            of(CARBON_DIOXIDE, 1, ENERGY_CELL, 2)),
    FUSION(
            of(HYDROGEN, 2, HELIUM, 1, NUCLEAR_REACTOR, 0, Immaterial.FUSION, 0),
            of(ENERGY_CELL, 30)
    ),

    ELECTROLYSIS_BLUEPRINT(
            of(RESEARCH, 50),
            of(ELECTROLYSIS, 1)
    ),
    STEEL_MAKING_BLUEPRINT(
            of(RESEARCH, 20),
            of(STEEL_MAKING, 1)
    ),
    FUSION_BLUEPRINT(
            of(RESEARCH, 200),
            of(Immaterial.FUSION, 1)
    ),

    HEAL_HUMAN(
            of(SICK_HUMAN, 1, CARBOHYDRATES, 3, ENERGY_CELL, 2),
            of(HUMAN, 1)
    ),
    HEAL_DRUHAN(
            of(SICK_DRUHAN, 1, HYDROGEN, 3, ENERGY_CELL, 4),
            of(DRUHAN, 1)
    ),
    HEAL_MHARR(
            of(SICK_MHARR, 1, CARBON, 6, ENERGY_CELL, 2),
            of(MHARR, 1)
    ),
    HEAL_THIOTITH(
            of(SICK_THIOTITH, 1, QUARTZ, 5, ENERGY_CELL, 3),
            of(THIOTITH, 1)
    ),

    REPAIR_NUCLEAR_REACTOR(
            of(BROKEN_NUCLEAR_REACTOR, 1, STEEL, 5, ENERGY_CELL, 2, Immaterial.FUSION, 0),
            of(NUCLEAR_REACTOR, 1)
    ),
    REPAIR_CHEMICAL_REACTOR(
            of(BROKEN_CHEMICAL_REACTOR, 1, IRON, 4, ENERGY_CELL, 2),
            of(CHEMICAL_REACTOR, 1)
    ),
    REPAIR_FUSION_DRIVE(
            of(BROKEN_FUSION_DRIVE, 1, STEEL, 5, ENERGY_CELL, 3, Immaterial.FUSION, 0),
            of(FUSION_DRIVE, 1)
    )

    ;

    fun description() = "recipe.${name.toLowerCase()}".i18n()

    companion object {
        fun recipesUsing(collectible: Collectible): VList<Recipe> = VList.of(*values())
                .filter { it.input.keySet().contains(collectible) }
    }

    fun bluePrintRecipe(): Option<Immaterial> =
            Option.of(output.first()._1)
                    .filter { output.size() == 1 }
                    .filter { it is Immaterial }
                    .map { it as Immaterial }
}