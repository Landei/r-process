package rprocess.model.resources

interface Collectible {
    val type: CollectibleType

    fun description(): String
}