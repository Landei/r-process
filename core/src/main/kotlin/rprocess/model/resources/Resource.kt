package rprocess.model.resources

import rprocess.model.resources.CollectibleType.*
import rprocess.util.i18n

enum class Resource(val maxPerCell: Int, override val type: CollectibleType, val damaged: Boolean = false) : Collectible {
    ENERGY_CELL(99, AGGREGATION),
    RESEARCH(99, AGGREGATION),
    CURRENCY(99, AGGREGATION),

    HUMAN(1, CREW),
    DRUHAN(1, CREW),
    MHARR(1, CREW),
    THIOTITH(1, CREW),

    SICK_HUMAN(1, CREW, true),
    SICK_DRUHAN(1, CREW, true),
    SICK_MHARR(1, CREW, true),
    SICK_THIOTITH(1, CREW, true),

    HYDROGEN(20, MATERIAL),
    HELIUM(20, MATERIAL),
    OXYGEN(20, MATERIAL),
    CARBON(15, MATERIAL),
    SILICON(10, MATERIAL),
    IRON(10, MATERIAL),
    URANIUM(20, MATERIAL),
    WATER(10, MATERIAL),
    CARBON_DIOXIDE(10, MATERIAL),
    QUARTZ(8, MATERIAL),
    IRON_ORE(10, MATERIAL),
    STEEL(10, MATERIAL),
    CARBOHYDRATES(15, MATERIAL),

    CHEMICAL_REACTOR(1, DEVICE),
    NUCLEAR_REACTOR(1, DEVICE),
    CHEMICAL_DRIVE(1, DEVICE),
    NUCLEAR_DRIVE(1, DEVICE),
    FUSION_DRIVE(1, DEVICE),
    STANDARD_SHIELD(1, DEVICE),

    BROKEN_CHEMICAL_REACTOR(1, DEVICE,true),
    BROKEN_NUCLEAR_REACTOR(1, DEVICE,true),
    BROKEN_CHEMICAL_DRIVE(1, DEVICE,true),
    BROKEN_NUCLEAR_DRIVE(1, DEVICE,true),
    BROKEN_FUSION_DRIVE(1, DEVICE,true),
    BROKEN_STANDARD_SHIELD(1, DEVICE, true)

    ;

    override fun description() = "resource.${name.toLowerCase()}".i18n()
}
