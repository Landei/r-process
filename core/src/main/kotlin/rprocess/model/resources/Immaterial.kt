package rprocess.model.resources

import rprocess.model.resources.CollectibleType.BLUE_PRINT
import rprocess.model.resources.CollectibleType.QUEST_ITEM
import rprocess.util.i18n

enum class Immaterial(override val type:CollectibleType) : Collectible {
    ELECTROLYSIS(BLUE_PRINT),
    STEEL_MAKING(BLUE_PRINT),
    FUSION(BLUE_PRINT),

    FLOWER_ROOT(QUEST_ITEM),
    FLOWER_LEAF(QUEST_ITEM),
    FLOWER_STALK(QUEST_ITEM),
    FLOWER_BLOSSOM(QUEST_ITEM)

    ;

    override fun description() = "immaterial.${name.toLowerCase()}".i18n()
}