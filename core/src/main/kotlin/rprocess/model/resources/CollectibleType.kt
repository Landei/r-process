package rprocess.model.resources

enum class CollectibleType {
    AGGREGATION,
    CREW,
    MATERIAL,
    DEVICE,
    BLUE_PRINT,
    QUEST_ITEM
}