package rprocess.model.resources

import rprocess.util.Lens

data class Deposit(val resource: Resource, val amount: Int, val hidden: Boolean = true) {

    companion object {
        val amountL = Lens(Deposit::amount) { deposit, amount -> deposit.copy(amount = amount)}
        val hiddenL = Lens(Deposit::hidden) { deposit, hidden -> deposit.copy(hidden = hidden)}
    }

}