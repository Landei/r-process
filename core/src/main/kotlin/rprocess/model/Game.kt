package rprocess.model

import rprocess.model.galaxy.Coord
import rprocess.model.galaxy.Galaxy
import rprocess.model.galaxy.Star
import rprocess.model.history.History
import rprocess.model.ship.Ship
import rprocess.util.Lens
import rprocess.util.mapL

data class Game(val ship: Ship, val galaxy: Galaxy, val history: History) {

    fun coord():Coord = history.coord()

    fun currentStar(): Star = galaxy.systems[coord()].get()

    companion object {
        val shipL = Lens(Game::ship) { game, ship -> game.copy(ship = ship) }
        val galaxyL = Lens(Game::galaxy) { game, galaxy -> game.copy(galaxy = galaxy) }
        val historyL = Lens(Game::history) { game, history -> game.copy(history = history) }
        val currentStarL = Lens(Game::currentStar)
        { game, star -> (galaxyL * Galaxy.systemsL * mapL<Coord, Star>(game.coord())).set(game, star) }
    }
}