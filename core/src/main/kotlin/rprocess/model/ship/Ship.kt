package rprocess.model.ship

import io.vavr.Tuple
import io.vavr.Tuple2
import io.vavr.control.Option
import io.vavr.kotlin.pair
import rprocess.model.Pos
import rprocess.model.galaxy.Body
import rprocess.model.galaxy.Route
import rprocess.model.resources.Collectible
import rprocess.model.resources.Immaterial
import rprocess.model.resources.Resource
import rprocess.util.Lens
import rprocess.util.mapL
import kotlin.math.min
import io.vavr.collection.Map as VMap
import io.vavr.collection.Set as VSet

data class Ship(
        val type: ShipType,
        val name: String,
        val cells: VMap<Pos, Cell>,
        val immaterials: VSet<Immaterial>) {

    fun cargoMap(): VMap<Pos, Cargo> = cells.filterValues { it is Cargo }.mapValues { it as Cargo }
    fun emptyMap(): VMap<Pos, EmptyCell> = cells.filterValues { it is EmptyCell }.mapValues { it as EmptyCell }

    fun inventory(): VMap<Collectible, Int> = cargoMap()
            .values()
            .groupBy { it.resource as Collectible }
            .mapValues { cargo -> cargo.sumBy(Cargo::amount) }
            .merge(immaterials.toMap { immaterial -> Tuple.of(immaterial, 1) })

    fun addResource(resource: Resource, count: Int): Option<Ship> {
        val result = addResourceIfPossible(resource, count)
        return Option.some(result._1).filter { result._2 == 0 }
    }

    fun addResourceIfPossible(resource: Resource, count: Int): Tuple2<Ship, Int> {
        if (count == 0) {
            return Tuple.of(this, 0)
        }
        val filledUpResult = cargoMap().filterValues { cargo -> cargo.resource == resource && cargo.amount < resource.maxPerCell }
                .sortedByDescending { tuple -> tuple._2.amount } //fill up bigger cells first
                .fold(Tuple.of(this, count)) { newResult, tuple ->
                    val remaining = newResult._2
                    when {
                        remaining > 0 -> {
                            val (pos, cargo) = tuple.pair()
                            val delta = min(resource.maxPerCell - cargo.amount, remaining)
                            Tuple.of(newResult._1.changeCargoAmount(pos, delta), remaining - delta)
                        }
                        else -> newResult
                    }
                }
        return emptyMap().filterValues { it == EmptyCell }
                .keySet()
                .sortedBy { it }
                .fold(filledUpResult) { newResult, pos ->
                    val remaining = newResult._2
                    when {
                        remaining > 0 -> {
                            val delta = min(resource.maxPerCell, remaining)
                            Tuple.of(newResult._1.putCargo(pos, Cargo(resource, delta)), remaining - delta)
                        }
                        else -> newResult
                    }
                }
    }

    fun removeResource(resource: Resource, count: Int): Option<Ship> {
        val result = cargoMap().filterValues { cargo -> cargo.resource == resource }
                .sortedBy { tuple -> tuple._2.amount }  //use up smaller cells first
                .fold(Tuple.of(this, count)) { newResult, tuple ->
                    val remaining = newResult._2
                    when {
                        remaining > 0 -> {
                            val (pos, cargo) = tuple.pair()
                            val delta = min(remaining, cargo.amount)
                            Tuple.of(newResult._1.changeCargoAmount(pos, -delta), remaining - delta)
                        }
                        else -> newResult
                    }
                }
        return Option.some(result._1).filter { result._2 == 0 }
    }

    fun putCargo(pos: Pos, cargo: Cargo): Ship {
        require(cells.containsKey(pos))
        return cellL(pos).set(this, cargo)
    }

    fun removeCargo(pos: Pos): Ship {
        require(cargoMap().containsKey(pos))
        return copy(cells = cells.put(pos, EmptyCell))
    }

    fun changeCargoAmount(pos: Pos, delta: Int): Ship {
        val cargo = cargoMap().get(pos).get()
        val newAmount = cargo.amount + delta
        return when {
            newAmount > cargo.resource.maxPerCell -> throw Exception("too much stuff")
            newAmount > 0 -> putCargo(pos, cargo.copy(amount = newAmount))
            newAmount == 0 -> removeCargo(pos)
            else -> throw Exception("negative cargo amount")
        }
    }

    fun availableSpace(resource: Resource) = cells.values().map { cell ->
        when {
            cell is EmptyCell -> resource.maxPerCell
            cell is Cargo && cell.resource == resource -> resource.maxPerCell - cell.amount
            else -> 0
        }
    }.sumBy { it }

    companion object {
        const val SCAN_ENERGY = 2
        const val EXTRACT_ENERGY = 3

        val nameL = Lens(Ship::name) { ship, name -> ship.copy(name = name) }
        val cellsL = Lens(Ship::cells) { ship, cells -> ship.copy(cells = cells) }
        val immaterialL = Lens(Ship::immaterials) { ship, immaterials -> ship.copy(immaterials = immaterials) }

        fun cellL(pos: Pos) = cellsL * mapL<Pos, Cell>(pos)
    }

    //Type dependent stuff

    fun scanProbability(body: Body): Float = type.scanProbability(this, body)

    fun canScan(body: Body): Boolean = type.canScan(this, body)

    fun scanned(body: Body): Ship = type.scanned(this, body)

    fun cellRepairMaterial(): VMap<Resource, Int> = type.cellRepairMaterial(this)

    fun resourceRepairMaterial(resource: Resource) : VMap<Collectible, Int> = type.resourceRepairMaterial(this, resource)

    fun extractProbability(body: Body, depth: Int, hidden: Boolean): Float = type.extractProbability(this, body, depth, hidden)

    fun canExtract(body: Body, depth: Int): Boolean = type.canExtract(this, body, depth)

    fun extracted(body: Body, depth: Int, extractedResources: VMap<Resource, Int>): Ship =
            type.extracted(this, body, depth, extractedResources)

    fun travelResources(route: Route): VMap<Resource, Int> = type.travelResources(this, route)

    fun canTravel(resources: VMap<Resource, Int>) = type.canTravel(this, resources)

    fun travel(route: Route): Ship = type.travel(this, route)

}