package rprocess.model.ship

import rprocess.model.resources.Resource

sealed class Cell {
    abstract val amount: Int
}

object EmptyCell : Cell() {
    override val amount: Int = 0
}
object DamagedCell : Cell() {
    override val amount: Int = 0
}

data class Cargo(val resource: Resource, override val amount: Int) : Cell() {
    init {
        require(0 < amount){
            "Cargo amount must be positive, got $amount"
        }
        require(amount <= resource.maxPerCell) {
            "$amount is too much cargo, $resource allows only ${resource.maxPerCell}"
        }
    }
}