package rprocess.model.ship

enum class Species(val description: String) {
    HUMAN("Human"),
    MHARR("M'Harr"),
    DRUHAN("DruHan"),
    THIOTITH("Thiotith")
}