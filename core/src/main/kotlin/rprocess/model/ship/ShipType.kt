package rprocess.model.ship

import io.vavr.kotlin.pair
import rprocess.model.galaxy.Body
import rprocess.model.galaxy.Route
import rprocess.model.resources.Collectible
import rprocess.model.resources.CollectibleType
import rprocess.model.resources.Immaterial
import rprocess.model.resources.Resource
import rprocess.util.i18n

import io.vavr.collection.HashMap as VHashMap
import io.vavr.collection.Map as VMap

enum class ShipType {
    FREIGHTER,
    EXPLORER

    ;

    fun description() = "ship.${name.toLowerCase()}".i18n()

    fun canScan(ship: Ship, body: Body) = ship.inventory()[Resource.ENERGY_CELL].getOrElse(0) >= Ship.SCAN_ENERGY

    fun scanned(ship: Ship, body: Body) = ship.removeResource(Resource.ENERGY_CELL, Ship.SCAN_ENERGY).get()

    fun scanProbability(ship: Ship, body: Body) = 0.5F

    fun cellRepairMaterial(ship: Ship): VMap<Resource, Int> = when (this) {
        FREIGHTER -> VHashMap.of(Resource.IRON, 1, Resource.ENERGY_CELL, 2)
        EXPLORER -> VHashMap.of(Resource.SILICON, 2, Resource.ENERGY_CELL, 1)
    }

    fun resourceRepairMaterial(ship: Ship, resource: Resource) : VMap<Collectible, Int> = when(resource) {
        Resource.HUMAN -> VHashMap.of(Resource.CARBOHYDRATES, 3, Resource.ENERGY_CELL, 2)
        Resource.DRUHAN -> VHashMap.of(Resource.HYDROGEN, 3, Resource.ENERGY_CELL, 4)
        Resource.MHARR -> VHashMap.of(Resource.CARBON, 6, Resource.ENERGY_CELL, 2)
        Resource.THIOTITH -> VHashMap.of(Resource.QUARTZ, 5, Resource.ENERGY_CELL, 3)
        Resource.NUCLEAR_REACTOR -> VHashMap.of(Resource.STEEL, 5, Resource.ENERGY_CELL, 2, Immaterial.FUSION, 0)
        Resource.CHEMICAL_REACTOR -> VHashMap.of(Resource.IRON, 4, Resource.ENERGY_CELL, 2)
        Resource.FUSION_DRIVE -> VHashMap.of(Resource.STEEL, 5, Resource.ENERGY_CELL, 3, Immaterial.FUSION, 0)
        else -> VHashMap.empty()
    }

    fun extractProbability(ship: Ship, body: Body, depth: Int, hidden: Boolean) = when {
        hidden -> 0.3F
        else -> 0.5F
    }

    fun canExtract(ship: Ship, body: Body, depth: Int) =
            ship.inventory()[Resource.ENERGY_CELL].getOrElse(0) >= Ship.EXTRACT_ENERGY

    fun extracted(ship: Ship, body: Body, depth: Int, extractedResources: VMap<Resource, Int>): Ship =
            extractedResources.fold(ship.removeResource(Resource.ENERGY_CELL, Ship.SCAN_ENERGY).get()) { newShip, tuple ->
                val (resource, amount) = tuple.pair()
                newShip.addResourceIfPossible(resource, amount)._1
            }

    fun travelResources(ship: Ship, route: Route): VMap<Resource, Int> = ship.inventory()
            .fold(shipTravelResources(route)) { map, tuple ->
                val (resource, amount) = tuple.pair()
                if (resource is Resource && resource.type == CollectibleType.CREW) {
                    val needed = crewTravelResources(resource, route).mapValues { it * amount }
                    map.merge(needed, Int::plus)
                } else map
            }.filterValues { it != 0 }

    fun travelProduced(ship: Ship, route: Route): VMap<Resource, Int> = ship.inventory()
            .fold(shipTravelResources(route)) { map, tuple ->
                val (resource, amount) = tuple.pair()
                if (resource is Resource && resource.type == CollectibleType.CREW) {
                    val needed = crewTravelProduced(resource, route).mapValues { it * amount }
                    map.merge(needed, Int::plus)
                } else map
            }.filterValues { it != 0 }

    fun canTravel(ship: Ship, resources: VMap<Resource, Int>) = resources.all { tuple ->
        val (resource, amount) = tuple.pair()
        ship.inventory().getOrElse(resource, 0) >= amount
    }

    fun travel(ship: Ship, route: Route): Ship {
        val shipRemoveResources = travelResources(ship, route).fold(ship) { ship, tuple ->
            val (resource, amount) = tuple.pair()
            when {
                resource is Resource && amount > 0 -> ship.removeResource(resource, amount).get()
                else -> ship
            }
        }
        return travelProduced(ship, route).fold(shipRemoveResources) { newShip, tuple ->
            val (resource, amount) = tuple.pair()
            when {
                resource is Resource -> newShip.addResourceIfPossible(resource, amount)._1
                else -> newShip
            }
        }
    }

    private fun crewTravelResources(resource: Resource, route: Route) = when (resource) {
        Resource.HUMAN -> VHashMap.of(
                Resource.OXYGEN, route.dist(),
                Resource.WATER, route.dist() / 2,
                Resource.CARBOHYDRATES, 2)
        Resource.MHARR -> VHashMap.of(
                Resource.CARBON_DIOXIDE, route.dist()
        )
        Resource.DRUHAN -> VHashMap.of(
                Resource.WATER, route.dist()
        )
        Resource.THIOTITH -> VHashMap.of(
                Resource.SILICON, route.dist() / 3
        )
        else -> VHashMap.empty()
    }

    private fun crewTravelProduced(resource: Resource, route: Route) = when (resource) {
        Resource.HUMAN -> VHashMap.of(
                Resource.RESEARCH, route.dist() * 10,
                Resource.CARBON_DIOXIDE, route.dist() / 2)
        Resource.MHARR -> VHashMap.of(
                Resource.RESEARCH, route.dist() * 8,
                Resource.CARBON, route.dist() / 3
        )
        Resource.DRUHAN -> VHashMap.of(
                Resource.RESEARCH, route.dist() * 12,
                Resource.OXYGEN, route.dist() / 2
        )
        Resource.THIOTITH -> VHashMap.of(
                Resource.RESEARCH, route.dist() * 5
        )
        else -> VHashMap.empty()
    }

    private fun shipTravelResources(route: Route) = VHashMap.of(Resource.ENERGY_CELL, route.dist())
}