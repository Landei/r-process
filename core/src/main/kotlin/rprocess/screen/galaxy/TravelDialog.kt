package rprocess.screen.galaxy

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import rprocess.Assets
import rprocess.Main
import rprocess.model.Game
import rprocess.model.galaxy.Coord
import rprocess.model.galaxy.Route
import rprocess.model.resources.Resource
import rprocess.render.*
import rprocess.render.actors.ModalContextActor
import rprocess.render.actors.TextButton
import rprocess.screen.starSystem.StarSystemScreen
import rprocess.util.i18n
import rprocess.util.toTypedArray
import rprocess.util.triple
import io.vavr.collection.HashMap as VHashMap
import io.vavr.collection.List as VList
import io.vavr.collection.Map as VMap

class TravelDialog(
        val game: Game,
        val coord: Coord,
        private val sourcePos: () -> Vector2) : ModalContextActor() {

    private val fixedPrimitives: Array<Primitive>
    private val bounds = Rectangle(0F, 0F, 700F, 400F).center()
    private val rayColor1 = Color(Color.SKY).apply { a = 0.1F }
    private val rayColor2 = Color(Color.SKY).apply { a = 0.2F }

    init {
        val galaxy = game.galaxy
        val currentCoord = game.coord()
        val ship = game.ship
        val system = galaxy.systems.get(coord).get()
        val title = "dialog.travel.title".i18n(system.name)
        val (titleWidth, titleHeight) = Assets.mainFont.size(title, 0.5F)
        val lineHeight = bounds.y + bounds.height - titleHeight - 40F
        val w2 = bounds.width / 2

        val dist = galaxy.dist(currentCoord, coord)
                .map { it.toString() }.getOrElse("?")

        val directConnection = galaxy.routes.exists { route -> route.between(coord, currentCoord) }
        val travelResources: VMap<Resource, Int> = if (directConnection) {
            ship.travelResources(Route(currentCoord, coord))
        } else VHashMap.empty()
        val canTravel = directConnection && ship.canTravel(travelResources)

        fixedPrimitives = arrayOf(
                Picture(Assets.dialog, bounds),
                Text(title, at(bounds.x + w2 - titleWidth / 2F, bounds.y + bounds.height - titleHeight), 0.5F, Color.GOLD),
                Text("dialog.travel.dist".i18n(system.starType.i18n(), dist), at(bounds.x + 50, bounds.y + bounds.height - 3.5F * titleHeight), 0.5F, Color.GOLD),
                Rect(bounds, Color.GOLD),
                Line(at(bounds.x, lineHeight), at(bounds.x + bounds.width, lineHeight), Color.GOLD),
                *requiredResources(travelResources))

        val buttonBounds = Rectangle(bounds.x, bounds.y + 20F, w2, titleHeight + 20F).pad(-20F, 0F)
        addActor(TextButton("dialog.travel.cancel".i18n(), buttonBounds) { it.parent.remove() })

        buttonBounds.x += w2
        addActor(TextButton("dialog.travel.start".i18n(), buttonBounds) { travel(Route(currentCoord, coord)) }.apply {
            touchable = toTouchable(canTravel)
        })
    }

    private fun requiredResources(resources: VMap<Resource, Int>): Array<Primitive> =
            resources.zipWithIndex().flatMap { tupleTuple ->
                val (res, required, index) = tupleTuple.triple()
                val available = game.ship.inventory().getOrElse(res, 0)
                val sprite = when (res) {
                    is Resource -> Assets.resource(res)
                    else -> Assets.unknownCell
                }
                val startX = bounds.x + (cellWidth + 20F) * index + 50F
                val grayScale = required > available
                VList.of(
                        Sprite(sprite, at(startX, bounds.y + 200F), grayScale),
                        Text("$required / $available", at(startX + 20F, bounds.y + 180F), 0.3F, Color.GOLD)
                )
            }.toTypedArray()

    override fun render(): Array<Primitive> = sourcePos.invoke().let { pos ->
        val p1 = Vector2(bounds.x, bounds.y)
        val p2 = Vector2(bounds.x + bounds.width, bounds.y)
        val p3 = Vector2(bounds.x + bounds.width, bounds.y + bounds.height)
        val p4 = Vector2(bounds.x, bounds.y + bounds.height)
        arrayOf(
                Triangle(p1, p2, pos, rayColor1, true),
                Triangle(p2, p3, pos, rayColor2, true),
                Triangle(p3, p4, pos, rayColor1, true),
                Triangle(p4, p1, pos, rayColor2, true),
                * fixedPrimitives
        )
    }

    private fun travel(route: Route) {
        val game1 = Game.shipL.set(game, game.ship.travel(route))
        val game2 = Game.historyL.modify(game1) { history -> history.visit(coord) }
        Main.saveGame(game2)
        Main.screen.dispose()
        Main.screen = StarSystemScreen(game2)
    }
}