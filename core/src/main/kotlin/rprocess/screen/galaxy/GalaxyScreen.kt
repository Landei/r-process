package rprocess.screen.galaxy

import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Event
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.InputListener
import io.vavr.control.Try
import org.pmw.tinylog.Logger
import rprocess.Assets
import rprocess.Main
import rprocess.model.Game
import rprocess.render.*
import rprocess.render.actors.ContextScreenAdapter
import rprocess.render.actors.navigationMenu
import rprocess.screen.menu.MenuScreen
import rprocess.util.sqr
import rprocess.util.toTypedArray

class GalaxyScreen(var game: Game) : ContextScreenAdapter() {

    private val mousePosListener = MousePosListener()

    init {
        Logger.info("Show Galaxy Screen")
        navigationMenu(this, ::GalaxyScreen, stage) { Try.success(game) }
        stage.addListener(::keyListener)
        stage.addListener(mousePosListener)
        addStars()
    }

    override fun renderPreStage(): Array<Primitive> {
        val mx = mousePosListener.x
        val my = mousePosListener.y
        val routes = game.galaxy.routes.map<Primitive> {
            val p1 = at(it.c1.x * 20 + Main.worldWidth / 2, it.c1.y * 20 + Main.worldHeight / 2)
            val p2 = at(it.c2.x * 20 + Main.worldWidth / 2, it.c2.y * 20 + Main.worldHeight / 2)
            val colour = when {
                game.history.hasUsed(it) -> Color.SKY
                it.contains(game.coord()) -> Color.GOLD
                else -> Color.GRAY
            }
            Line(distort(p1, mx, my), distort(p2, mx, my), colour)
        }.toTypedArray()

        return arrayOf(
                Picture(Assets.stars1, Main.worldRect),
                *routes
        )
    }

    private fun keyListener(event: Event) = when {
        event is InputEvent && event.type == InputEvent.Type.keyDown && event.keyCode == Input.Keys.ESCAPE -> {
            dispose()
            Main.screen = MenuScreen(::GalaxyScreen, Try.success(game))
            true
        }
        else -> false
    }

    private fun addStars() {
        game.galaxy.systems.keySet().forEach { coord ->
            stage.addActor(SystemActor(game, coord, mousePosListener))
        }
    }

    class MousePosListener : InputListener() {
        var x: Float = 0F
        var y: Float = 0F

        override fun mouseMoved(inputEvent: InputEvent, x: Float, y: Float): Boolean {
            this.x = inputEvent.stageX
            this.y = inputEvent.stageY
            return false
        }
    }

    companion object {
        fun distort(v: Vector2, mx: Float, my: Float): Vector2 = when {
            Main.worldRect.pad(-3F, -3F).contains(mx, my) && Main.mouseInsideWindow() -> {
                val dist = v.dst(mx, my) + 3
                val drag = 100 / (1 + sqr(sqr(2 - dist / 100)))
                at(v.x + drag * (v.x - mx) / dist, v.y + drag * (v.y - my) / dist)
            }
            else -> v
        }
    }
}

