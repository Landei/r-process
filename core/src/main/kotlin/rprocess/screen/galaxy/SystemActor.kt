package rprocess.screen.galaxy

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Circle
import rprocess.Assets
import rprocess.Main
import rprocess.Sounds
import rprocess.model.Game
import rprocess.model.galaxy.Coord
import rprocess.render.CircularSprite
import rprocess.render.Primitive
import rprocess.render.Text
import rprocess.render.actors.LeftClickListener
import rprocess.render.actors.RoundContextActor
import rprocess.render.at
import rprocess.screen.galaxy.GalaxyScreen.Companion.distort


class SystemActor(private val game: Game,
                  private val coord: Coord,
                  private val mousePosListener: GalaxyScreen.MousePosListener) : RoundContextActor() {

    private val system = game.galaxy.systems.get(coord).get()
    private val pos = at(coord.x * 20 + Main.worldWidth / 2, coord.y * 20 + Main.worldHeight / 2)
    private val sprite = CircularSprite(Assets.star(system.starType), system.radius / 10F)

    override val bounds = Circle(pos.x, pos.y, system.radius / 10F)

    init {
        addListener(LeftClickListener {
            Sounds.click()
            travel()
        })
    }

    override fun render(): Array<Primitive> {
        val p = distort(pos, mousePosListener.x, mousePosListener.y)
        val dist = p.dst2(mousePosListener.x, mousePosListener.y)
        val text: Array<Primitive> = when {
            dist < 300 * 300 || coord == game.coord() -> arrayOf(
                    Text(system.name, at(p.x + 11F, p.y + 19F), 0.4F, Color.BLACK),
                    Text(system.name, at(p.x + 10F, p.y + 20F), 0.4F, textColor()))
            else -> arrayOf()
        }
        sprite.sprite.setOriginBasedPosition(p.x, p.y)
        return arrayOf(
                *text,
                sprite
        )
    }

    private fun textColor() = when {
        coord == game.coord() -> Color.GOLD
        game.history.path.contains(coord) -> Color.SKY
        else -> Color.GRAY
    }

    private fun travel() {
        stage.addActor(TravelDialog(game, coord) { distort(pos, mousePosListener.x, mousePosListener.y) })
    }

}