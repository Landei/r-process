package rprocess.screen.starSystem

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Rectangle
import io.vavr.kotlin.pair
import rprocess.Assets
import rprocess.Sounds
import rprocess.model.Game
import rprocess.model.ship.Cargo
import rprocess.model.ship.DamagedCell
import rprocess.render.*
import rprocess.render.actors.ContextActor
import rprocess.render.actors.HooverListener
import rprocess.render.actors.LeftClickListener
import rprocess.util.toTypedArray
import io.vavr.collection.List as VList

class ShipSymbolActor (
        val game: Game,
        val bounds: Rectangle,
        action: () -> Unit) : ContextActor() {

    private val picture = Picture(Assets.ship, bounds)
    private val hooverListener = HooverListener(this)

    init {
        setBounds(bounds)
        addListener(LeftClickListener {
            Sounds.click()
            action.invoke()
        })
   }

    override fun render(): Array<Primitive> = arrayOf(picture,
            * cellsOnHoover())

    private fun cellsOnHoover(): Array<Primitive> = when{
        hooverListener.hoovering() -> {
            game.ship.cells
                    .flatMap { tuple ->
                        val (pos, cell) = tuple.pair()
                        val xk = bounds.x + pos.x * cellWidth + 75
                        val yk = bounds.y + pos.y * cellHeight + 75
                        val cellPos = at(xk + 1, yk + 1)
                        val damaged: VList<Primitive> = when {
                            cell is Cargo && cell.resource.damaged -> VList.of(Sprite(Assets.damaged, cellPos))
                            cell is DamagedCell -> VList.of(Sprite(Assets.damaged, cellPos))
                            else -> VList.empty()
                        }

                        when (cell) {
                            is Cargo -> VList.of(
                                    Sprite(Assets.resource(cell.resource), cellPos),
                                    Rect(xk + 3,  yk + cellWidth - 20F, cellWidth / 2F - 5F, 17F,
                                            Color(Color.BLACK).apply { a = 0.6F }, true),
                                    Text(String.format("%2d", cell.amount),
                                            at(xk + 6, yk + cellHeight - 4),
                                            0.3F,
                                            textColor(cell))).appendAll(damaged)
                            else -> VList.of<Primitive>(Sprite(Assets.emptyCell, cellPos)).appendAll(damaged)
                        }
                    }.toTypedArray()
        }
        else -> arrayOf()
    }

    private fun textColor(cargo: Cargo) = when {
        cargo.amount == cargo.resource.maxPerCell -> Color.GOLD
        else -> Color.WHITE
    }
}