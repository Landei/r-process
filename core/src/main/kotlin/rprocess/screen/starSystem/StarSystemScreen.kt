package rprocess.screen.starSystem

import com.badlogic.gdx.Input.Keys.ESCAPE
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.Event
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.InputEvent.Type.keyDown
import io.vavr.control.Try
import org.pmw.tinylog.Logger
import rprocess.Assets
import rprocess.Main
import rprocess.Main.worldRect
import rprocess.model.Game
import rprocess.model.galaxy.Body
import rprocess.model.galaxy.Moon
import rprocess.model.galaxy.Planet
import rprocess.model.galaxy.Star
import rprocess.render.*
import rprocess.render.actors.ContextScreenAdapter
import rprocess.render.actors.navigationMenu
import rprocess.screen.menu.MenuScreen
import rprocess.screen.ship.ShipScreen
import com.badlogic.gdx.math.Circle as GdxCircle


class StarSystemScreen(var game: Game) : ContextScreenAdapter() {

    private val shipBounds = Rectangle(100F, 50F, 80F, 50F)

    private val primitivesPreStage = arrayOf<Primitive>(Picture(Assets.stars1, worldRect))

    private val primitivesPostStage = arrayOf<Primitive>(
            Text(star().name, at(367F, 878F), 0.5F, Color.BLACK),
            Text(star().name, at(365F, 880F), 0.5F, Color.GOLD))

    init {
        Logger.info("Show StarSystem Screen")
        registerStar()
        navigationMenu(this, ::StarSystemScreen, stage) { Try.success(game) }
        stage.addActor(ShipSymbolActor(game, shipBounds){
            this@StarSystemScreen.dispose()
            Main.screen = ShipScreen(game)
        })
        stage.addListener(::keyListener)
    }

    private fun registerStar() {
        stage.addActor(StarActor(star(), spriteBody(star())) { at ->
            stage.addActor(BodyDialog(star(), game, ::setter) { at })
        })
        star().planets.forEach(::registerPlanet)
    }

    private fun registerPlanet(planet: Planet) {
        PlanetActor(planet, shipBounds, spriteBody(planet)) { at ->
            val p = Star.planetByNameL(planet.name)(star())
            stage.addActor(BodyDialog(p, game, ::setter) { at })
        }.also { actor ->
            stage.addActor(actor)
            planet.moons.forEach { registerMoon(it, actor.bounds) }
        }
    }

    private fun registerMoon(moon: Moon, planetBounds: GdxCircle) {
        stage.addActor(MoonActor(moon, planetBounds, spriteBody(moon)) { at ->
            val m = Star.moonByNameL(moon.name)(star())
            stage.addActor(BodyDialog(m, game, ::setter, at))
        })
    }

    private fun star() = game.currentStar()

    override fun renderPreStage(): Array<Primitive> = primitivesPreStage

    override fun renderPostStage(): Array<Primitive> = primitivesPostStage

    private fun keyListener(event: Event) = when {
        event is InputEvent && event.type == keyDown && event.keyCode == ESCAPE -> {
            dispose()
            Main.screen = MenuScreen(::StarSystemScreen, Try.success(game))
            true
        }
        else -> false
    }

    private fun spriteBody(body: Body) = CircularSprite(
            when (body) {
                is Star -> Assets.star(body.starType)
                is Planet -> Assets.planet(body.planetType)
                is Moon -> Assets.moon(body.moonType)
            }, body.radius)

    private fun setter(newGame: Game) {
        game = newGame
        Main.saveGame(game)
    }
}