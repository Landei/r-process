package rprocess.screen.starSystem

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Touchable
import io.vavr.Tuple
import io.vavr.control.Option
import io.vavr.kotlin.pair
import rprocess.Assets
import rprocess.model.Game
import rprocess.model.Pos
import rprocess.model.galaxy.Body
import rprocess.model.galaxy.Moon
import rprocess.model.galaxy.Planet
import rprocess.model.galaxy.Star
import rprocess.model.resources.Deposit
import rprocess.model.resources.Resource
import rprocess.render.*
import rprocess.render.actors.ModalContextActor
import rprocess.render.actors.TextButton
import rprocess.util.i18n
import rprocess.util.toTypedArray
import kotlin.random.Random
import io.vavr.collection.HashMap as VHashMap

class BodyDialog(private var body: Body,
                 var game: Game,
                 val setter: (Game) -> Unit,
                 private val sourcePos: () -> Vector2) : ModalContextActor() {

    private val title: String = when (body) {
        is Star -> "dialog.body.star".i18n()
        is Planet -> "dialog.body.planet".i18n()
        is Moon -> "dialog.body.moon".i18n()
    } + " ${body.name}"
    private val bounds: Rectangle = Rectangle(0F, 0F, 600F, 620F).center()
    private val scanActor: Actor
    private val miningActors: Array<Actor>
    private val fixedPrimitives: Array<Primitive>
    private val rayColor1 = Color(Color.SKY).apply { a = 0.1F }
    private val rayColor2 = Color(Color.SKY).apply { a = 0.2F }

    init {
        val titleMeasures = Assets.mainFont.size(title, 0.5F)
        val titleWidth = titleMeasures.first
        val titleHeight = titleMeasures.second

        val w2 = bounds.width / 2F
        val lineHeight = bounds.y + bounds.height - titleHeight - 40F
        fixedPrimitives = arrayOf(
                //background
                Picture(Assets.dialog, bounds),
                Rect(bounds, Color.GOLD),
                //title
                Line(at(bounds.x, lineHeight), at(bounds.x + bounds.width, lineHeight), Color.GOLD),
                Text(title, at(bounds.x + (bounds.width - titleWidth) / 2F, bounds.y + bounds.height - titleHeight), 0.5F, Color.GOLD))

        val leftBounds = Rectangle(bounds.x, bounds.y + 20F, w2, titleHeight + 20F).pad(-20F, 0F)
        val closeActor = TextButton("dialog.body.close".i18n(), leftBounds) { it.parent.remove() }
        addActor(closeActor)

        val rightBounds = Rectangle(bounds.x + w2, bounds.y + 20F, w2, titleHeight + 20F).pad(-20F, 0F)
        scanActor = TextButton("dialog.body.scan".i18n(), rightBounds) { scan() }
        addActor(scanActor)

        miningActors = Array(6) { y ->
            val bounds = Rectangle(bounds.x + w2, bounds.y + 95F + cellWidth * (5 - y), w2, titleHeight + 20F).pad(-20F, 0F)
            TextButton("dialog.body.extract".i18n(), bounds) { extract(y) }
                    .also {
                        addActor(it)
                        touchable = toTouchable(game.ship.canExtract(body, y))
                    }
        }

        scanActor.touchable = toTouchable(game.ship.canScan(body))
        //TODO disable scanning if everything is already scanned

        miningActors.forEachIndexed { y, actor ->
            actor.isVisible = body.deposits.keySet().any { pos -> pos.y == y }
        }

    }

    override fun render() = sourcePos.invoke().let { pos ->
        val p1 = Vector2(bounds.x, bounds.y)
        val p2 = Vector2(bounds.x + bounds.width, bounds.y)
        val p3 = Vector2(bounds.x + bounds.width, bounds.y + bounds.height)
        val p4 = Vector2(bounds.x, bounds.y + bounds.height)
        arrayOf(
                Triangle(p1, p2, pos, rayColor1, true),
                Triangle(p2, p3, pos, rayColor2, true),
                Triangle(p3, p4, pos, rayColor1, true),
                Triangle(p4, p1, pos, rayColor2, true),
                * fixedPrimitives,
                * renderDeposits()
        )
    }

    private fun renderDeposits(): Array<Primitive> = Pos.inRange(2, 5)
            .map { pos -> renderDeposit(pos, body.deposits[pos], Vector2(bounds.x + 2, bounds.y + 73)) }
            .toTypedArray()

    private fun renderDeposit(pos: Pos, depositOpt: Option<Deposit>, offset: Vector2): Primitive {
        val cellPos = at(offset.x + pos.x * cellWidth + 1, offset.y + (5 - pos.y) * cellHeight + 1)
        return depositOpt.map { deposit ->
            val sprite = if (deposit.hidden) Assets.unknownCell else Assets.resource(deposit.resource)
            Sprite(sprite, cellPos)
        }.getOrElse { Sprite(Assets.emptyCell, cellPos) }
    }

    private fun scan() {
        val probability = game.ship.scanProbability(body)

        val newDeposits = body.deposits.mapValues {
            when {
                Random.nextDouble(1.0) < probability -> it.copy(hidden = false)
                else -> it
            }
        }

        body = body.let {
            when (it) {
                is Star -> it.copy(deposits = newDeposits)
                is Planet -> it.copy(deposits = newDeposits)
                is Moon -> it.copy(deposits = newDeposits)
            }
        }

        val lens = Game.currentStarL * when (body) {
            is Star -> Star.depositsL
            is Planet -> Star.planetByNameL(body.name) * Planet.depositsL
            is Moon -> Star.moonByNameL(body.name) * Moon.depositsL
        }

        val newDepositGame = lens.set(game, newDeposits)
        game = Game.shipL.set(newDepositGame, game.ship.scanned(body))
        setter(game)

        scanActor.touchable = if (game.ship.canScan(body)) Touchable.enabled else Touchable.disabled
    }

    private fun extract(depth: Int) {
        val (newDeposits, extractedMaterial) = body.deposits.fold(
                Tuple.of(VHashMap.empty<Pos, Deposit>(), VHashMap.empty<Resource, Int>())) { accu, tuple ->
            val (pos, deposit) = tuple.pair()
            val (newDeposits, extracted) = accu.pair()
            if (pos.y == depth) {
                val probability = game.ship.extractProbability(body, depth, deposit.hidden)
                val extractedAmount = (1..deposit.amount).map {
                    if (Random.nextDouble(1.0) < probability) 1 else 0
                }.sumBy { it }
                val newDepositAmount = deposit.amount - extractedAmount
                Tuple.of(if (newDepositAmount == 0) newDeposits else newDeposits.put(pos, deposit.copy(amount = newDepositAmount)),
                        extracted.put(deposit.resource, extractedAmount + extracted.getOrElse(deposit.resource, 0)))
            } else {
                accu.map1 { it.put(pos, deposit) }
            }
        }.pair()

        body = body.let {
            when (it) {
                is Star -> it.copy(deposits = newDeposits)
                is Planet -> it.copy(deposits = newDeposits)
                is Moon -> it.copy(deposits = newDeposits)
            }
        }

        val lens = Game.currentStarL * when (body) {
            is Star -> Star.depositsL
            is Planet -> Star.planetByNameL(body.name) * Planet.depositsL
            is Moon -> Star.moonByNameL(body.name) * Moon.depositsL
        }

        val newDepositGame = lens.set(game, newDeposits)
        game = Game.shipL.set(newDepositGame, game.ship.extracted(body, depth, extractedMaterial))
        setter(game)
        miningActors[depth].touchable = toTouchable(game.ship.canExtract(body, depth))

    }
}

