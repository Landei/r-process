package rprocess.screen.starSystem

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Circle
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import rprocess.Assets
import rprocess.Main.worldHeight
import rprocess.Main.worldRect
import rprocess.Main.worldWidth
import rprocess.Sounds
import rprocess.model.galaxy.Planet
import rprocess.render.CircularSprite
import rprocess.render.DottedCircle
import rprocess.render.Primitive
import rprocess.render.actors.LeftClickListener
import rprocess.render.actors.RoundContextActor
import rprocess.render.pad
import kotlin.math.PI
import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.sin
import kotlin.random.Random

class PlanetActor(planet: Planet,
                  ship: Rectangle,
                  private val sprite: CircularSprite,
                  action: (Vector2) -> Unit) : RoundContextActor() {

    override val bounds: Circle
    private val orbit = DottedCircle(worldWidth, worldHeight, planet.distance, Color.GOLD)
    private val shadow = CircularSprite(Assets.shadow, planet.radius)

    init {
        val planetBound = planet.moons.lastOption().map { it.distance + it.radius }.getOrElse(planet.radius)
        val paddedWorld = worldRect.pad(-planetBound, -planetBound)
        val paddedShip = ship.pad(planetBound, planetBound)
        var cx: Float
        var cy: Float
        do {
            val theta = (Random.nextInt(90) * PI / 180).toFloat()
            cx = worldWidth - planet.distance * cos(theta)
            cy = worldHeight - planet.distance * sin(theta)
        } while (!paddedWorld.contains(cx, cy) || paddedShip.contains(cx, cy))

        bounds = Circle(cx, cy, planet.radius)
        sprite.sprite.setOriginBasedPosition(cx, cy)
        shadow.sprite.setOriginBasedPosition(cx, cy)
        val theta = (atan2(1.0 * worldHeight - cy, 1.0* worldWidth - cx) * 180 / PI).toFloat()
        shadow.sprite.rotate(90 + theta)

        addListener(LeftClickListener {
            Sounds.click()
            action.invoke(Vector2(bounds.x, bounds.y))
        })
    }

    override fun act(delta: Float) {
        super.act(delta)
        sprite.sprite.rotation = (sprite.sprite.rotation + 5 * delta) % 360
    }

    override fun render(): Array<Primitive> = arrayOf(orbit, sprite, shadow)
}