package rprocess.screen.starSystem

import com.badlogic.gdx.math.Circle
import com.badlogic.gdx.math.Vector2
import rprocess.Main.worldHeight
import rprocess.Main.worldWidth
import rprocess.Sounds
import rprocess.model.galaxy.Star
import rprocess.render.CircularSprite
import rprocess.render.Primitive
import rprocess.render.actors.LeftClickListener
import rprocess.render.actors.RoundContextActor

class StarActor(star: Star,
                private val sprite: CircularSprite,
                private val action: (Vector2) -> Unit) : RoundContextActor() {

    override val bounds = Circle(worldWidth, worldHeight, star.radius)

    init {
        sprite.sprite.setOriginBasedPosition(worldWidth, worldHeight)
        addListener(LeftClickListener {
            Sounds.click()
            action.invoke(Vector2(worldWidth, worldHeight))
        })
    }

    override fun act(delta: Float) {
        super.act(delta)
        sprite.sprite.rotation = (sprite.sprite.rotation + delta) % 360
    }

    override fun render(): Array<Primitive> = arrayOf(sprite)
}