package rprocess.screen.starSystem

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Circle
import com.badlogic.gdx.math.Vector2
import rprocess.Assets
import rprocess.Main
import rprocess.Sounds
import rprocess.model.galaxy.Moon
import rprocess.render.CircularSprite
import rprocess.render.DottedCircle
import rprocess.render.Primitive
import rprocess.render.actors.LeftClickListener
import rprocess.render.actors.RoundContextActor
import kotlin.math.PI
import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.sin
import kotlin.random.Random

class MoonActor(private var moon: Moon,
                private val pBound: Circle,
                private val sprite: CircularSprite,
                action: (() -> Vector2) -> Unit) : RoundContextActor() {

    override val bounds = Circle(0F, 0F, moon.radius)
    private var theta = (Random.nextInt(360) * PI / 180).toFloat()
    private val orbit = DottedCircle(pBound.x, pBound.y, moon.distance, Color.GOLD)

    private val shadow = CircularSprite(Assets.shadow, moon.radius)

    init {
        recalcBounds()
        addListener(LeftClickListener {
            Sounds.click()
            action.invoke { Vector2(bounds.x, bounds.y) }
        })
    }

    private fun recalcBounds() {
        bounds.x = pBound.x + moon.distance * cos(theta)
        bounds.y = pBound.y + moon.distance * sin(theta)

        sprite.sprite.setOriginBasedPosition(bounds.x, bounds.y)
        shadow.sprite.setOriginBasedPosition(bounds.x, bounds.y)
        val theta = (atan2(1.0 * Main.worldHeight - bounds.y, 1.0* Main.worldWidth - bounds.x) * 180 / PI).toFloat()
        shadow.sprite.rotation = 90 + theta
    }

    override fun act(delta: Float) {
        theta += 0.02F * delta % (2 * PI).toFloat()
        recalcBounds()
        sprite.sprite.rotation = (sprite.sprite.rotation + 10 * delta) % 360
        super.act(delta)
    }

    override fun render(): Array<Primitive> = arrayOf(orbit, sprite, shadow)
}