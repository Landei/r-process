package rprocess.screen.menu

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.MathUtils.random
import com.badlogic.gdx.math.Rectangle
import io.vavr.control.Try
import org.pmw.tinylog.Logger
import rprocess.Assets
import rprocess.Main
import rprocess.Main.worldRect
import rprocess.Settings
import rprocess.Sounds
import rprocess.generators.GameG
import rprocess.model.Game
import rprocess.render.*
import rprocess.render.actors.ConfirmDialog
import rprocess.render.actors.ContextScreenAdapter
import rprocess.render.actors.InfoDialog
import rprocess.screen.starSystem.StarSystemScreen
import rprocess.util.i18n
import java.io.FileNotFoundException

class MenuScreen(private val lastScreen: (Game) -> ScreenAdapter, val game: Try<Game>) : ContextScreenAdapter() {

    private val primitives = arrayOf(Picture(Assets.stars, worldRect),
            Text("r-process", at(284F, 696F), 1.5F, Color.BLACK),
            Text("r-process", at(280F, 700F), 1.5F, Color.CORAL))

    init {
        Logger.info("Show Menu Screen")

        val rect = Rectangle(260F, 315F, 400F, 50F)
        stage.addActor(MenuButton("menu.continue", rect, this::resumeAction).also {
            it.touchable = toTouchable(!game.isEmpty)
        })
        rect.y -= 80
        stage.addActor(MenuButton("menu.new_game", rect, this::newGameAction))
        rect.y -= 80
        stage.addActor(MenuButton("menu.options", rect, this::optionsAction))
        rect.y -= 80
        stage.addActor(MenuButton("menu.exit", rect, this::exitAction))

        if (game.isFailure && game.cause !is FileNotFoundException) {
            stage.addActor(InfoDialog(
                    "dialog.load_error.title".i18n(),
                    "dialog.load_error.text".i18n(),
                    "dialog.load_error.button".i18n(),
                    Rectangle(0F, 0F, 700F, 200F).center()))
        }
        Sounds.menuBackground.play()
    }

    override fun renderPreStage(): Array<Primitive> = primitives

    private fun resumeAction() {
        dispose()
        Main.screen = lastScreen(game.get())
    }

    private fun newGameAction() {
        val action = {
            val newGame = GameG.generate(GameG.Difficulty.EASY, GameG.GalaxySize.MEDIUM, random(Long.MAX_VALUE))
            Main.saveGame(newGame)
            dispose()
            Main.screen = StarSystemScreen(newGame)
        }

        //TODO bring up dialog with difficulty, galaxy size and optionally seed
        if (game.isSuccess) {
            stage.addActor(ConfirmDialog(
                    "dialog.new_game.overwrite".i18n(),
                    "dialog.new_game.no".i18n(),
                    "dialog.new_game.yes".i18n(),
                    Rectangle(0F, 0F, 600F, 200F).center(),
                    { it.parent.remove() }, { action() }))
        } else {
            action()
        }
    }

    private fun optionsAction() {
        stage.addActor(OptionsDialog())
    }

    private fun exitAction() {
        stage.addActor(ConfirmDialog(
                "dialog.exit_game.exit".i18n(),
                "dialog.exit_game.cancel".i18n(),
                "dialog.exit_game.leave".i18n(),
                Rectangle(0F, 0F, 600F, 200F).center(),
                { it.parent.remove() },
                {
                    dispose()
                    Settings.save()
                    Main.dispose()
                    Gdx.app.exit()
                }))
    }

    override fun dispose() {
        Sounds.menuBackground.stop()
        super.dispose()
    }
}