package rprocess.screen.menu

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Rectangle
import rprocess.Assets
import rprocess.Settings
import rprocess.render.*
import rprocess.render.actors.ModalContextActor
import rprocess.render.actors.OptionButton
import rprocess.render.actors.TextButton
import rprocess.util.Property
import rprocess.util.i18n
import java.util.*

class OptionsDialog : ModalContextActor() {

    private var locale = Settings.locale
    private val primitives : Array<Primitive>

    init {
        val title = "dialog.options.title".i18n()
        val bounds = Rectangle(0F, 0F, 600F, 400F).center()
        val (titleWidth, titleHeight) = Assets.mainFont.size(title, 0.5F)
        val lineHeight = bounds.y + bounds.height - titleHeight - 40F
        val w2 = bounds.width / 2

        primitives = arrayOf(
                Picture(Assets.dialog, bounds),
                Text(title, at(bounds.x + w2 - titleWidth / 2F, bounds.y + bounds.height - titleHeight), 0.5F, Color.GOLD),
                Rect(bounds, Color.GOLD),
                Line(at(bounds.x, lineHeight), at(bounds.x + bounds.width, lineHeight), Color.GOLD))

        val buttonBounds = Rectangle(bounds.x, bounds.y + 20F, w2, titleHeight + 20F).pad(-20F, 0F)
        addActor(TextButton("dialog.options.cancel".i18n(), buttonBounds) { it.parent.remove() })

        buttonBounds.x += w2
        addActor(TextButton("dialog.options.save".i18n(), buttonBounds) { save() })

        val localeProperty = object : Property<Locale>(){
            override fun invoke() = locale
            override fun invoke(a: Locale) {
                locale = a
            }
        }

        val langBounds = Rectangle(bounds.x, bounds.y + bounds.height - 120F, bounds.width, titleHeight + 20F).pad(-20F, 0F)
        addActor(OptionButton("English", langBounds, Locale.ENGLISH, localeProperty))

        langBounds.y -= 50
        addActor(OptionButton("Deutsch", langBounds, Locale.GERMAN, localeProperty))
    }

    override fun render(): Array<Primitive> = primitives

    private fun save() {
        Settings.locale = locale
        Settings.save()
        remove()
    }
}