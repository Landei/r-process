package rprocess.screen.menu

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.Touchable
import rprocess.Sounds
import rprocess.render.*
import rprocess.render.actors.ContextActor
import rprocess.render.actors.HooverListener
import rprocess.render.actors.LeftClickListener
import rprocess.util.i18n

class MenuButton(private val titleKey: String,
                 rect: Rectangle,
                 action: () -> Unit) : ContextActor() {

    private val primitives: () -> Array<Primitive>

    init {
        setBounds(rect)
        val hooverListener = HooverListener(this)
        addListener(LeftClickListener{
            Sounds.click()
            action.invoke()
        })

        val h = height / 2F
        primitives = {
            val col = when {
                touchable == Touchable.disabled -> Color.SLATE
                hooverListener.hoovering() -> Color.GOLD
                else -> Color.WHITE
            }
            arrayOf(Text(titleKey.i18n(), at(x + width / 2F - 100F, y + h + 13F), 0.5F, col),
                    Line(at(0F, y + h), at(x, y + h), col),
                    Hexagon(x, y, width, height, col))
        }
    }

    override fun render(): Array<Primitive> = primitives()
}