package rprocess.screen.ship

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.Actor
import rprocess.Assets
import rprocess.model.Game
import rprocess.model.resources.CollectibleType
import rprocess.model.resources.Immaterial
import rprocess.render.*
import rprocess.render.actors.ModalContextActor
import rprocess.render.actors.TextButton
import rprocess.render.layout.*
import rprocess.util.Property
import rprocess.util.i18n
import java.lang.Integer.min
import io.vavr.collection.HashMap as VHashMap
import io.vavr.collection.List as VList

class BluePrintSelection(val game: Property<Game>) : ModalContextActor() {

    private val primitives: Array<Primitive>
    private val bounds = Rectangle(950F, 50F, 600F, 800F)

    val upButton: Actor
    val downButton: Actor
    var fromIndex = 0
    val rows = 12
    var bluePrints = VList.empty<Actor>()
    val listArea: Rectangle

    init {
        val vLayout = vSplitter("top", fixed(50F), "middle", prop(), "bottom", fixed(70F))
        val bottomLayout = hSplitter("left", prop(40F), "middle", prop(30F), "right", prop(30F))
        val layout = Nester(vLayout, VHashMap.of("bottom", bottomLayout)).layout(bounds)

        listArea = layout["middle"].get()
        val titlePos = layout["top"].get().let { at(it.x + 10F, it.y + 35F) }

        primitives = arrayOf(
                Picture(Assets.dialog, bounds),
                Rect(bounds, Color.GOLD),
                Rect(layout["top"].get(), Color.GOLD),
                Text("dialog.blue_print_selection.title".i18n(), titlePos, 0.5F, Color.WHITE)
        )

        val leftBounds = layout["bottom.left"].get().pad(-10F, -15F)
        addActor(TextButton("dialog.blue_print_selection.close".i18n(), leftBounds) { it.parent.remove() })
        val middleBounds = layout["bottom.middle"].get().pad(-10F, -15F)
        upButton = TextButton("dialog.blue_print_selection.up".i18n(), middleBounds)
        { fromIndex -= rows; scroll() }.also { addActor(it) }
        val rightBounds = layout["bottom.right"].get().pad(-10F, -15F)
        downButton = TextButton("dialog.blue_print_selection.down".i18n(), rightBounds)
        { fromIndex += rows; scroll() }.also { addActor(it) }
        scroll()
    }

    private fun scroll() {
        bluePrints.forEach { it.remove() }
        val bluePrints = game().ship.immaterials.filter { it.type == CollectibleType.BLUE_PRINT }
                .toList().sorted(Comparator.comparing<Immaterial, String>{ it.name })
        upButton.touchable = toTouchable(0 < fromIndex)
        downButton.touchable = toTouchable(fromIndex + rows < bluePrints.size())
        var top = listArea.y + listArea.height - 60F
        this.bluePrints = VList.ofAll(bluePrints.subSequence(fromIndex, min(fromIndex + rows, bluePrints.size()))
                .map { bluePrint ->
                    TextButton(bluePrint.description(), Rectangle(listArea.x, top, listArea.width, 50F).pad(-10F, -5F))
                    { stage.addActor(SelectRecipeDialog(bluePrint, game)) }.also { addActor(it); top -= 55F }
                })
    }

    override fun render() = primitives

}