package rprocess.screen.ship

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.Actor
import io.vavr.kotlin.pair
import rprocess.Assets
import rprocess.model.Game
import rprocess.model.Pos
import rprocess.model.resources.Resource
import rprocess.model.ship.EmptyCell
import rprocess.model.ship.Ship
import rprocess.render.*
import rprocess.render.actors.ModalContextActor
import rprocess.render.actors.TextButton
import rprocess.util.Property
import rprocess.util.i18n
import rprocess.util.toTypedArray
import rprocess.util.triple
import kotlin.math.max
import io.vavr.collection.List as VList

class RepairCellDialog(private val pos: Pos, private val game: Property<Game>) : ModalContextActor() {

    private val bounds: Rectangle = Rectangle(0F, 0F, 600F, 350F).center()
    private val primitives: Array<Primitive>

    init {
        val title = "dialog.repair_cell.title".i18n()
        val titleMeasures = Assets.mainFont.size(title, 0.5F)
        val titleWidth = titleMeasures.first
        val titleHeight = titleMeasures.second
        val w2 = bounds.width / 2F
        val lineHeight = bounds.y + bounds.height - titleHeight - 40F

        primitives = arrayOf(
                //background
                Picture(Assets.dialog, bounds),
                Rect(bounds, Color.GOLD),
                //title
                Line(at(bounds.x, lineHeight), at(bounds.x + bounds.width, lineHeight), Color.GOLD),
                Text(title, at(bounds.x + (bounds.width - titleWidth) / 2F, bounds.y + bounds.height - titleHeight), 0.5F, Color.GOLD),
                Text("dialog.repair_cell.required_material".i18n(), at(bounds.x + 20, bounds.y + bounds.height - titleHeight - 70), 0.5F, Color.GOLD),
                *inputs()
        )

        val leftBounds = Rectangle(bounds.x, bounds.y + 20F, w2, titleHeight + 20F).pad(-20F, 0F)
        addActor(TextButton("dialog.repair_cell.cancel".i18n(), leftBounds) { it.parent.remove() })
        val rightBounds = Rectangle(bounds.x + w2, bounds.y + 20F, w2, titleHeight + 20F).pad(-20F, 0F)
        addActor(TextButton("dialog.repair_cell.repair".i18n(), rightBounds, ::repair).apply{
            touchable = toTouchable(canRepair())
        })
     }

    override fun render(): Array<Primitive> = primitives

    private fun inputs(): Array<Primitive> = game().ship.cellRepairMaterial().zipWithIndex().flatMap{ tupleTuple ->
        val (res, needed, index) = tupleTuple.triple()
        val available = game().ship.inventory().getOrElse(res, 0)
        val sprite = when (res) {
            is Resource -> Assets.resource(res)
            else -> Assets.unknownCell
        }
        val startX = bounds.x + (cellWidth + 20F) * index + 50F
        val grayScale = needed != 0 && available < needed
        VList.of(
                Sprite(sprite, at(startX, bounds.y + bounds.height - 220F), grayScale),
                Text("${max(1, needed)} / $available", at(startX + 20F, bounds.y + bounds.height - 240F), 0.3F, Color.GOLD)
        )
    }.toTypedArray()

    private fun canRepair() = game().ship.cellRepairMaterial().all { tuple ->
        val (res, needed) = tuple.pair()
        val available = game().ship.inventory().getOrElse(res, 0)
        available >= needed
    }

    private fun repair(actor: Actor) {
        val newShip: Ship = game().ship.cellRepairMaterial().foldLeft(game().ship) { s, tuple ->
            val (res, amount) = tuple.pair()
            s.removeResource(res, amount).get()
        }
        game.modify { Game.shipL.set(it, newShip.copy(cells = newShip.cells.put(pos, EmptyCell))) }
        actor.parent.remove()
    }
}