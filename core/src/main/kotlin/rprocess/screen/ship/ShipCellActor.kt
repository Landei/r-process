package rprocess.screen.ship

import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import rprocess.Assets.ship
import rprocess.model.Game
import rprocess.render.Picture
import rprocess.render.Primitive
import rprocess.render.actors.ContextActor
import rprocess.render.actors.GroupContextActor
import rprocess.render.setBounds
import rprocess.util.Property

class ShipCellActor(private val bounds: Rectangle,
                    cellOffset: Vector2,
                    game: Property<Game>) : GroupContextActor() {

    private val primitives: Array<Primitive> = arrayOf(Picture(ship, bounds))

    private var renderAfter: Array<Primitive> = arrayOf()

    init {
        setBounds(bounds)
        val offset = Vector2(cellOffset.x, cellOffset.y)
        game().ship.cells.keySet().sortedBy { -1000 * it.x + it.y }.forEach { pos ->
            addActor(CargoActor(pos, offset, bounds, game) { array -> renderAfter = array })
        }
        addActor(object : ContextActor() {
            override fun render(): Array<Primitive> {
                val array = renderAfter.copyOf()
                renderAfter = arrayOf()
                return array
            }
        })
    }

    override fun render(): Array<Primitive> = primitives

}