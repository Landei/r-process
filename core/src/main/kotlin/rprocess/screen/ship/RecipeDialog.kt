package rprocess.screen.ship

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Touchable
import io.vavr.kotlin.pair
import rprocess.Assets
import rprocess.model.Game
import rprocess.model.resources.Immaterial
import rprocess.model.resources.Recipe
import rprocess.model.resources.Resource
import rprocess.render.*
import rprocess.render.actors.ModalContextActor
import rprocess.render.actors.TextButton
import rprocess.util.Property
import rprocess.util.i18n
import rprocess.util.toTypedArray
import rprocess.util.triple
import kotlin.math.max
import kotlin.math.min
import io.vavr.collection.List as VList

class RecipeDialog(private val recipe: Recipe, val game: Property<Game>) : ModalContextActor() {

    private val bounds: Rectangle = Rectangle(0F, 0F, 800F, 620F).center()
    private val primitives: Array<Primitive>
    private val oneButton: Actor
    private val fiveButton: Actor
    private val tenButton: Actor
    private val fiftyButton: Actor

    init {
        val title = recipe.description()
        val titleMeasures = Assets.mainFont.size(title, 0.5F)
        val titleWidth = titleMeasures.first
        val titleHeight = titleMeasures.second
        val w2 = bounds.width / 2F
        val lineHeight = bounds.y + bounds.height - titleHeight - 40F

        primitives = arrayOf(
                //background
                Picture(Assets.dialog, bounds),
                Rect(bounds, Color.GOLD),
                //title
                Line(at(bounds.x, lineHeight), at(bounds.x + bounds.width, lineHeight), Color.GOLD),
                Text(title, at(bounds.x + (bounds.width - titleWidth) / 2F, bounds.y + bounds.height - titleHeight), 0.5F, Color.GOLD),
                Text("dialog.use_recipe.used_amounts".i18n(), at(bounds.x + 20, bounds.y + bounds.height - titleHeight - 70), 0.5F, Color.GOLD),
                Text("dialog.use_recipe.produced_amounts".i18n(), at(bounds.x + 20, bounds.y + bounds.height - titleHeight - 250), 0.5F, Color.GOLD)
        )

        val oneBounds = Rectangle(bounds.x, bounds.y + 80F, w2 / 2F, titleHeight + 20F).pad(-20F, 0F)
        oneButton = TextButton("1 x", oneBounds) { applyRecipe(1) }
        addActor(oneButton)
        val fiveBounds = Rectangle(bounds.x + w2 / 2F, bounds.y + 80F, w2 / 2F, titleHeight + 20F).pad(-20F, 0F)
        fiveButton = TextButton("5 x", fiveBounds) { applyRecipe(5) }
        addActor(fiveButton)
        val tenBounds = Rectangle(bounds.x + w2, bounds.y + 80F, w2 / 2F, titleHeight + 20F).pad(-20F, 0F)
        tenButton = TextButton("10 x", tenBounds) { applyRecipe(10) }
        addActor(tenButton)
        val fiftyBounds = Rectangle(bounds.x + w2 * 1.5F, bounds.y + 80F, w2 / 2F, titleHeight + 20F).pad(-20F, 0F)
        fiftyButton = TextButton("50 x", fiftyBounds) { applyRecipe(50) }
        addActor(fiftyButton)
        val leftBounds = Rectangle(bounds.x, bounds.y + 20F, w2, titleHeight + 20F).pad(-20F, 0F)
        addActor(TextButton("dialog.use_recipe.close".i18n(), leftBounds) { it.parent.remove() })
    }

    private fun inputs() = recipe.input.zipWithIndex().flatMap { tupleTuple ->
        val (res, needed, index) = tupleTuple.triple()
        val available = game().ship.inventory().getOrElse(res, 0)
        val sprite = when (res) {
            is Resource -> Assets.resource(res)
            is Immaterial -> Assets.blueprint
            else -> Assets.unknownCell
        }
        val startX = bounds.x + (cellWidth + 20F) * index + 50F
        val grayScale = needed != 0 && available < needed
        VList.of(
                Sprite(sprite, at(startX, bounds.y + bounds.height - 220F), grayScale),
                Text("${max(1, needed)} / $available", at(startX + 20F, bounds.y + bounds.height - 240F), 0.3F, Color.GOLD)
        )
    }.toTypedArray()

    private fun outputs() = recipe.output.zipWithIndex().flatMap { tupleTuple ->
        val (res, produced, index) = tupleTuple.triple()
        val available = game().ship.inventory().getOrElse(res, 0)
        val sprite = when (res) {
            is Resource -> Assets.resource(res)
            is Immaterial -> Assets.blueprint
            else -> Assets.unknownCell
        }
        val startX = bounds.x + (cellWidth + 20F) * index + 50F
        val grayScale = maxOutput() == 0
        VList.of(
                Sprite(sprite, at(startX, bounds.y + bounds.height - 400F), grayScale),
                Text("$produced / $available", at(startX + 20F, bounds.y + bounds.height - 420F), 0.3F, Color.GOLD)
        )
    }.toTypedArray()

    override fun act(delta: Float) {
        val maxOutput = maxOutput()
        oneButton.touchable = if (maxOutput >= 1) Touchable.enabled else Touchable.disabled
        fiveButton.touchable = if (maxOutput >= 5) Touchable.enabled else Touchable.disabled
        tenButton.touchable = if (maxOutput >= 10) Touchable.enabled else Touchable.disabled
        fiftyButton.touchable = if (maxOutput >= 50) Touchable.enabled else Touchable.disabled
        super.act(delta)
    }

    override fun render() = arrayOf(
            *primitives,
            *inputs(),
            *outputs())

    private fun maxOutput(): Int = recipe.input.fold(Int.MAX_VALUE) { accu, tuple ->
        val (resource, needed) = tuple.pair()
        val available = game().ship.inventory().getOrElse(resource, 0)
        val factor = when {
            available == 0 -> 0
            needed == 0 -> Int.MAX_VALUE
            else -> available / needed
        }
        min(accu, factor)
    }.let { amount ->
        recipe.bluePrintRecipe()
                .map { if (game().ship.immaterials.contains(it)) 0 else min(1, amount) }
                .getOrElse { amount }
    }

    private fun applyRecipe(times: Int) {
        val shipUsed = recipe.input.fold(game().ship) { ship, tuple ->
            val (resource, amount) = tuple.pair()
            if (resource is Resource && amount > 0) {
                ship.removeResource(resource, amount * times).get()
            } else {
                ship
            }
        }
        val shipUsedAndProduced = recipe.output.fold(shipUsed) { ship, tuple ->
            val (collectible, amount) = tuple.pair()
            when (collectible) {
                is Resource -> ship.addResourceIfPossible(collectible,amount * times)._1
                is Immaterial -> ship.copy(immaterials = ship.immaterials.add(collectible))
                else -> error("impossible")
            }
        }
        game(Game.shipL.set(game(), shipUsedAndProduced))
    }
}