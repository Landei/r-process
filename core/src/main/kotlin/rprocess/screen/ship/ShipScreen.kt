package rprocess.screen.ship

import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import io.vavr.control.Try
import rprocess.Assets
import rprocess.Main
import rprocess.model.Game
import rprocess.render.Picture
import rprocess.render.Primitive
import rprocess.render.actors.ContextScreenAdapter
import rprocess.render.actors.TextButton
import rprocess.render.actors.navigationMenu
import rprocess.util.Property
import rprocess.util.i18n

class  ShipScreen(var game: Game) : ContextScreenAdapter() {

    private val primitivesPreStage = arrayOf<Primitive>(Picture(Assets.stars1, Main.worldRect))
    private val shipBounds = Rectangle(100F, 50F, 800F, 500F)


    init {
        navigationMenu(this, ::ShipScreen, stage) { Try.success(game) }
        stage.addActor(ShipCellActor(shipBounds, Vector2(230F, 95F), gameProperty()))

        val selectBounds = Rectangle(400F, 850F, 400F, 40F)
        stage.addActor(TextButton("ship.selection.recipes".i18n(), selectBounds){})
        selectBounds.y -= 60F
        stage.addActor(TextButton("ship.selection.blue_prints".i18n(), selectBounds){
            stage.addActor(BluePrintSelection(gameProperty())) })
        selectBounds.y -= 60F
        stage.addActor(TextButton("ship.selection.quest_items".i18n(), selectBounds){
            stage.addActor(QuestItemSelection(gameProperty())) })
        selectBounds.y -= 60F
        stage.addActor(TextButton("ship.selection.stats".i18n(), selectBounds){})
        selectBounds.y -= 60F
        stage.addActor(TextButton("ship.selection.logs".i18n(), selectBounds){})
    }

    override fun renderPreStage(): Array<Primitive> = primitivesPreStage


    private fun setter(newGame: Game) {
        game = newGame
        Main.saveGame(game)
    }

    private fun gameProperty() = object : Property<Game>() {
        override fun invoke(): Game = game

        override fun invoke(a: Game) {
            setter(a)
        }
    }
}