package rprocess.screen.ship

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Rectangle
import io.vavr.Tuple
import io.vavr.kotlin.pair
import rprocess.Assets
import rprocess.model.Game
import rprocess.model.resources.Collectible
import rprocess.model.resources.Recipe
import rprocess.model.resources.Recipe.Companion.recipesUsing
import rprocess.render.*
import rprocess.render.actors.ModalContextActor
import rprocess.render.actors.TextButton
import rprocess.util.Property
import rprocess.util.i18n
import kotlin.math.min

class SelectRecipeDialog(collectible: Collectible, val game: Property<Game>) : ModalContextActor() {

    private val primitives: Array<Primitive>

    init {
        val bounds: Rectangle = Rectangle(0F, 0F, 850F, 620F).center()
        val title = "dialog.select_recipe.title".i18n(collectible.description())
        val titleMeasures = Assets.mainFont.size(title, 0.5F)
        val titleWidth = titleMeasures.first

        recipesUsing(collectible)
                .filter { recipe -> recipe
                        .bluePrintRecipe()
                        .map{ ! game().ship.immaterials.contains(it) }
                        .getOrElse { true } }
                .map { recipe -> Tuple.of(recipe, maxOutput(recipe)) }
                .sortBy { tuple -> -tuple._2 }
                .take(8)
                .forEachWithIndex { tuple, index ->
                    val yHeight = bounds.y + bounds.height - 20F - 100F - index * 2.5F * 20F
                    val receipeBounds = Rectangle(bounds.x, yHeight, bounds.width, 40F).pad(-20F, 0F)
                    addActor(TextButton(tuple._1.description() + " x " + tuple._2, receipeBounds) {
                        showRecipe(tuple._1)
                    })
                }

        val lineHeight = bounds.y + bounds.height - 60F
        primitives = arrayOf(
                //background
                Picture(Assets.dialog, bounds),
                Rect(bounds, Color.GOLD),
                //title
                Line(at(bounds.x, lineHeight), at(bounds.x + bounds.width, lineHeight), Color.GOLD),
                Text(title, at(bounds.x + (bounds.width - titleWidth) / 2F, bounds.y + bounds.height - 20F), 0.5F, Color.GOLD)
        )

        val leftBounds = Rectangle(bounds.x, bounds.y + 20F, bounds.width / 2F, 40F).pad(-20F, 0F)
        addActor(TextButton("dialog.select_recipe.close".i18n(), leftBounds) { it.parent.remove() })
    }

    private fun maxOutput(recipe: Recipe): Int = recipe.input.fold(Int.MAX_VALUE) { accu, tuple ->
        val (resource, needed) = tuple.pair()
        val available = game().ship.inventory().getOrElse(resource, 0)
        val factor = when {
            available == 0 -> 0
            needed == 0 -> Int.MAX_VALUE
            else -> available / needed
        }
        min(accu, factor)
    }.let { amount ->
        recipe.bluePrintRecipe()
                .map{ min(1, amount) }
                .getOrElse{amount}
    }

    private fun showRecipe(recipe: Recipe) {
        stage.addActor(RecipeDialog(recipe, game))
        remove()
    }

    override fun render() = primitives
}