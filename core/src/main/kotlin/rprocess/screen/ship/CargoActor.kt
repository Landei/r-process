package rprocess.screen.ship

import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.utils.DragListener
import rprocess.Assets
import rprocess.Sounds
import rprocess.model.Game
import rprocess.model.Pos
import rprocess.model.resources.CollectibleType
import rprocess.model.ship.Cargo
import rprocess.model.ship.DamagedCell
import rprocess.model.ship.EmptyCell
import rprocess.render.*
import rprocess.render.actors.ConfirmDialog
import rprocess.render.actors.ContextActor
import rprocess.render.actors.HooverListener
import rprocess.render.actors.LeftClickListener
import rprocess.util.Property
import rprocess.util.i18n

class CargoActor(val pos: Pos,
                 offset: Vector2,
                 val bounds: Rectangle,
                 val game: Property<Game>,
                 val dragFrame: (Array<Primitive>) -> Unit) : ContextActor() {

    private val xk = offset.x + pos.x * cellWidth
    private val yk = offset.y + pos.y * cellHeight
    private val cellPos = at(xk + 1, yk + 1)

    private val dragListener = dragListener()
    private val hooverListener = HooverListener(this)
    private val description: String = game().ship.cells.get(pos)
            .filter { it is Cargo }
            .map { (it as Cargo).resource.description() }
            .getOrElse("")
    private val textMeasures: Pair<Float, Float>

    init {
        textMeasures = Assets.mainFont.size(description, 0.4F)
        setBounds(xk + 1, yk + 1, cellWidth - 2, cellHeight - 2)
        addListener(dragListener)
        addListener(LeftClickListener {
            Sounds.click()
            clicked()
        })
    }

    private fun dragListener(): DragListener = object : DragListener() {

        init {
            button = Input.Buttons.RIGHT
        }

        override fun dragStop(event: InputEvent, x: Float, y: Float, pointer: Int) {
            if (!bounds.contains(x + bounds.x + xk, y + bounds.y + yk) && isDroppable()) {
                val cargo = game().ship.cargoMap().get(pos).get()
                stage.addActor(ConfirmDialog(
                        "dialog.drop_cargo.title".i18n(cargo.amount, cargo.resource.description()),
                        "dialog.drop_cargo.cancel".i18n(),
                        "dialog.drop_cargo.dispose".i18n(),
                        Rectangle(0F, 0F, 700F, 200F).center(),
                        { it.parent.remove() },
                        {
                            val newShip = game().ship.removeCargo(pos)
                            game(Game.shipL.set(game(), newShip))
                            it.parent.remove()
                        }))
            }
        }
    }

    private fun isDroppable() = game().ship.cargoMap().get(pos)
            .map { it.resource.type }
            .filter { it != CollectibleType.CREW }
            .isDefined

    override fun render(): Array<Primitive> = game().ship.cells.get(pos)
            .map { cell ->
                if (dragListener.isDragging && isDroppable()) {
                    val colour =
                            if (bounds.contains(
                                            dragListener.dragX + bounds.x + xk,
                                            dragListener.dragY + bounds.y + yk)) Color.GREEN
                            else Color.FIREBRICK
                    dragFrame(arrayOf(
                            Rect(dragListener.dragX + bounds.x + xk - cellWidth / 2,
                                    dragListener.dragY + bounds.y + yk - cellHeight / 2,
                                    cellWidth, cellHeight,
                                    colour)))
                }
                val damaged: Array<Primitive> = when {
                    cell is Cargo && cell.resource.damaged -> arrayOf(Sprite(Assets.damaged, cellPos))
                    cell is DamagedCell -> arrayOf(Sprite(Assets.damaged, cellPos))
                    else -> arrayOf()
                }
                val hoover: Array<Primitive> = when {
                    hooverListener.hoovering() && cell is Cargo -> arrayOf(
                            Rect(bounds.x + xk, bounds.y + yk - 20F, textMeasures.first + 6, 21F,
                                    Color(Color.BLACK).apply { a = 0.8F }, true),
                            Text(description, at(xk + 3, yk), 0.4F, Color.WHITE))
                    else -> arrayOf()
                }

                when (cell) {
                    is Cargo -> arrayOf(
                            Sprite(Assets.resource(cell.resource), cellPos),
                            Rect(bounds.x + xk + 3, bounds.y + yk + cellWidth - 20F, cellWidth / 2F - 5F, 17F,
                                    Color(Color.BLACK).apply { a = 0.6F }, true),
                            Text(String.format("%2d", cell.amount),
                                    at(xk + 6, yk + cellHeight - 4),
                                    0.3F,
                                    textColor(cell)),
                            *damaged,
                            *hoover)
                    else -> arrayOf(Sprite(Assets.emptyCell, cellPos),
                            *damaged)
                }
            }.get()

    private fun textColor(cargo: Cargo) = when {
        cargo.amount == cargo.resource.maxPerCell -> Color.GOLD
        else -> Color.WHITE
    }

    private fun clicked() {
        game().ship.cells.get(pos)
                .forEach {
                    when (it) {
                        EmptyCell -> println("empty")
                        DamagedCell -> stage.addActor(RepairCellDialog(pos, game))
                        is Cargo -> stage.addActor(SelectRecipeDialog(it.resource, game))
                    }
                }
    }
}