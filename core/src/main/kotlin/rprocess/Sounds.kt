package rprocess

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.Disposable

object Sounds : Disposable {

    private const val PATH = "core/assets/sounds"

    val buttonClick = Gdx.audio.newSound(Gdx.files.internal("$PATH/button.mp3"))

    val menuBackground = Gdx.audio.newMusic(Gdx.files.internal("$PATH/menu_loop.ogg"))

    init {
        menuBackground.isLooping = true
        menuBackground.volume = 0.2F
    }

    fun click() {
        buttonClick.play(1.0F)
    }

    override fun dispose() {
        buttonClick.dispose()
        menuBackground.dispose()
    }
}