package rprocess

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.Texture.TextureFilter
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.graphics.g2d.TextureRegion
import rprocess.model.galaxy.MoonType
import rprocess.model.galaxy.PlanetType
import rprocess.model.galaxy.StarType
import rprocess.model.resources.Resource
import rprocess.render.handle


object Assets : AssetManager() {

    val mainFont: BitmapFont

    private val resourceAtlas = TextureAtlas(Gdx.files.internal("core/assets/resources.atlas"))

    init {
        load("core/assets/background/stars.jpg", Texture::class.java)
        load("core/assets/background/stars1.jpg", Texture::class.java)
        load("core/assets/background/dialog.png", Texture::class.java)
        load("core/assets/system/STAR_WHITE.png", Texture::class.java)
        load("core/assets/system/STAR_BLUE.png", Texture::class.java)
        load("core/assets/system/STAR_YELLOW.png", Texture::class.java)
        load("core/assets/system/STAR_GREEN.png", Texture::class.java)
        load("core/assets/system/STAR_RED.png", Texture::class.java)
        load("core/assets/system/STAR_VIOLET.png", Texture::class.java)
        load("core/assets/system/STAR_BROWN.png", Texture::class.java)
        load("core/assets/system/PLANET_DESERT.png", Texture::class.java)
        load("core/assets/system/PLANET_HOT.png", Texture::class.java)
        load("core/assets/system/PLANET_ICY.png", Texture::class.java)
        load("core/assets/system/PLANET_MAGMA.png", Texture::class.java)
        load("core/assets/system/PLANET_OCEAN.png", Texture::class.java)
        load("core/assets/system/PLANET_PART_OCEAN.png", Texture::class.java)
        load("core/assets/system/PLANET_GAS_GIANT.png", Texture::class.java)
        load("core/assets/system/PLANET_ROCKY.png", Texture::class.java)
        load("core/assets/system/MOON_ROCKY.png", Texture::class.java)
        load("core/assets/system/MOON_ICY.png", Texture::class.java)
        load("core/assets/system/MOON_HOT.png", Texture::class.java)
        load("core/assets/system/MOON_MAGMA.png", Texture::class.java)
        load("core/assets/system/shadow.png", Texture::class.java)
        load("core/assets/ship.png", Texture::class.java)
        load("core/assets/icon/galaxy.png", Texture::class.java)
        load("core/assets/icon/mainMenu.png", Texture::class.java)
        load("core/assets/icon/system.png", Texture::class.java)
        load("core/assets/icon/ship.png", Texture::class.java)
        finishLoading()

        val fontTexture = Texture(handle("font/xolonium.png"), true)
        fontTexture.setFilter(TextureFilter.MipMapLinearNearest, TextureFilter.Linear)
        mainFont = BitmapFont(handle("font/xolonium.fnt"), TextureRegion(fontTexture), false)
    }

    val stars: Texture
        get() = get("core/assets/background/stars.jpg")

    val stars1: Texture
        get() = get("core/assets/background/stars1.jpg")

    val dialog: Texture
        get() = get("core/assets/background/dialog.png")

    val planet: Texture
        get() = get("core/assets/system/moho.jpg")

    val ship: Texture
        get() = get("core/assets/ship.png")

    val shadow: Texture
        get() = get("core/assets/system/shadow.png")

    val mainMenuIcon: Texture
        get() = get("core/assets/icon/mainMenu.png")

    val galaxyIcon: Texture
        get() = get("core/assets/icon/galaxy.png")

    val starSystemIcon: Texture
        get() = get("core/assets/icon/system.png")

    val shipIcon: Texture
        get() = get("core/assets/icon/ship.png")

    val emptyCell: Sprite
        get() = resourceAtlas.createSprite("EMPTY")

    val damaged: Sprite
        get() = resourceAtlas.createSprite("DAMAGED")

    val unknownCell: Sprite
        get() = resourceAtlas.createSprite("UNKNOWN")

    val blueprint: Sprite
        get() = resourceAtlas.createSprite("BLUE_PRINT")

    fun star(starType: StarType): Texture = get("core/assets/system/STAR_${starType.name}.png")

    fun planet(planetType: PlanetType): Texture = get("core/assets/system/PLANET_${planetType.name}.png")

    fun moon(moonType: MoonType): Texture = get("core/assets/system/MOON_${moonType.name}.png")

    fun resource(resource: Resource) : Sprite = resourceAtlas.createSprite(
            resource.name.replace("BROKEN_","").replace("SICK_",""))

    override fun dispose() {
        resourceAtlas.dispose()
        mainFont.dispose()
    }

}