package rprocess

import com.badlogic.gdx.Game
import com.badlogic.gdx.math.Rectangle
import io.vavr.control.Try
import org.pmw.tinylog.Configurator
import org.pmw.tinylog.Level
import org.pmw.tinylog.Logger
import org.pmw.tinylog.writers.ConsoleWriter
import org.pmw.tinylog.writers.FileWriter
import rprocess.screen.menu.MenuScreen
import rprocess.screen.starSystem.StarSystemScreen
import rprocess.util.GameJson
import rprocess.util.gson
import java.io.File
import java.io.FileNotFoundException
import java.nio.file.Paths
import rprocess.model.Game as RGame

object Main : Game() {

    private const val saveGame = "./game/saved.json"
    const val worldWidth = 1600F
    const val worldHeight = 900F
    val worldRect = Rectangle(0F, 0F, worldWidth, worldHeight)
    lateinit var mouseInsideWindow: () -> Boolean

    init {
        defaultExceptionHandler()
        configureLog()
        setI18N()
    }

    private fun defaultExceptionHandler() {
        Thread.setDefaultUncaughtExceptionHandler { thread, ex ->
            Logger.error("Error in ${thread.name}: $ex")
            if (ex.cause != null) {
                Logger.error(ex.cause)
            }
            ex.printStackTrace()
        }
    }

    private fun configureLog() {
        Configurator.defaultConfig()
                .writer(ConsoleWriter())
                .addWriter(FileWriter("server.log"))
                .level(Level.INFO)
                .activate()
    }

    private fun setI18N() {
        Settings.locale = Settings.locale //init the resource bundle
    }

    override fun create() {
        screen = MenuScreen(::StarSystemScreen, loadGame())
    }

    private fun loadGame(): Try<RGame> = Paths.get(saveGame).toFile().let {
        when {
            it.exists() -> Try.of{
                    gson.fromJson(it.readText(Charsets.UTF_8), GameJson::class.java).toGame()}
            else -> Try.failure(FileNotFoundException())
        }
    }

    fun saveGame(game: RGame) {
        val json = gson.toJson(GameJson(game))
        File(saveGame).writeText(json, Charsets.UTF_8)
    }

    override fun dispose() {
        Assets.dispose()
        super.dispose()
    }
}
