package rprocess.render

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import com.badlogic.gdx.math.Vector2
import rprocess.Assets

data class Text(val text: String,
                val at: Vector2,
                val scale: Float,
                val colour: Color,
                val font: BitmapFont = Assets.mainFont) : Primitive {
    override fun render(context: Context) {
         context.withBatch {
             useFont(this, font, scale, colour) { font ->
                 font.draw(this, text, at.x, at.y)
             }
         }
    }

    private fun useFont(batch: Batch, font: BitmapFont, scale: Float, color: Color, action: (BitmapFont) -> Unit) {
        batch.shader = fontShader
        font.data.setScale(scale)
        font.color = color
        action.invoke(font)
        batch.shader = null
    }

    companion object {
        private val fontShader: ShaderProgram = ShaderProgram(handle("font/font.vert"), handle("font/font.frag")).also {
            if (!it.isCompiled) {
                Gdx.app.error("fontShader", "compilation failed:\n" + it.log)
            }
        }
    }
}