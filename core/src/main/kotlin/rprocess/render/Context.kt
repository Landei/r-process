package rprocess.render

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer

class Context(private val batch: Batch) : AutoCloseable {

    private val batchInitiallyDrawing = batch.isDrawing

    fun withBatch(action: Batch.() -> Unit) {
        if (sr.isDrawing) {
            sr.end()
        }
        if (!batch.isDrawing) {
            batch.begin()
        }
        action.invoke(batch)
    }

    fun withPointSR(action: ShapeRenderer.() -> Unit) {
        if (batch.isDrawing) {
            batch.end()
        }
        if (sr.isDrawing && sr.currentType != ShapeRenderer.ShapeType.Point) {
            sr.end()
        }
        if (!sr.isDrawing) {
            sr.begin(ShapeRenderer.ShapeType.Point)
        }
        action.invoke(sr)
    }

    fun withLineSR(action: ShapeRenderer.() -> Unit) {
        if (batch.isDrawing) {
            batch.end()
        }
        if (sr.isDrawing && sr.currentType != ShapeRenderer.ShapeType.Line) {
            sr.end()
        }
        if (!sr.isDrawing) {
            sr.begin(ShapeRenderer.ShapeType.Line)
        }
        action.invoke(sr)
    }

    fun withFilledSR(blend: Boolean, action: ShapeRenderer.() -> Unit) {
        if (batch.isDrawing) {
            batch.end()
        }
        if (sr.isDrawing && sr.currentType != ShapeRenderer.ShapeType.Filled) {
            sr.end()
        }
        if (!sr.isDrawing) {
            sr.begin(ShapeRenderer.ShapeType.Filled)
        }
        if (blend && !Gdx.gl.glIsEnabled(GL20.GL_BLEND)) {
            Gdx.gl.glEnable(GL20.GL_BLEND)
        }
        if (!blend && Gdx.gl.glIsEnabled(GL20.GL_BLEND)) {
            Gdx.gl.glDisable(GL20.GL_BLEND)
        }
        action.invoke(sr)
    }

    fun withFilledSR(action: ShapeRenderer.() -> Unit) {
        withFilledSR(false, action)
    }

    fun draw(vararg primitives: Primitive) {
        primitives.forEach { it.render(this) }
    }

    fun drawAndClose(vararg primitives: Primitive) {
        this.use {
            draw(* primitives)
        }
    }

    fun pause() {
        if (sr.isDrawing) {
            sr.end()
        }
        if (batch.isDrawing) {
            batch.end()
        }
    }

    override fun close() {
        if (sr.isDrawing) {
            sr.end()
        }
        if (batch.isDrawing && !batchInitiallyDrawing) {
            batch.end()
        }
        if (!batch.isDrawing && batchInitiallyDrawing) {
            batch.begin()
        }
    }

    companion object {
        lateinit var sr: ShapeRenderer
    }

}