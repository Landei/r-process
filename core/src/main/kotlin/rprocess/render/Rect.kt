package rprocess.render

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Rectangle

data class Rect(val x: Float,
                val y: Float,
                val width: Float,
                val height: Float,
                val colour: Color,
                val filled: Boolean = false) : Primitive {

    constructor(r: Rectangle, colour: Color, filled: Boolean = false) :
            this(r.x, r.y, r.width, r.height, colour, filled)

    override fun render(context: Context) {
        val action = { sr: ShapeRenderer ->
            sr.color = colour
            sr.rect(x, y, width, height)
        }
        when {
            filled -> context.withFilledSR(colour.a != 1F, action)
            else -> context.withLineSR(action)
        }
    }
}