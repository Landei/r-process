package rprocess.render

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.glutils.ShapeRenderer

data class Hexagon(val x: Float, val y: Float, val width: Float, val height: Float, val colour: Color, val filled: Boolean = false) : Primitive {
    override fun render(context: Context) {
        val h = height / 2
        val action = { sr: ShapeRenderer ->
            sr.color = colour
            sr.polygon(floatArrayOf(
                    x, y + h,
                    x + h, y + height,
                    x + width - h, y + height,
                    x + width, y + h,
                    x + width - h, y,
                    x + h, y
            ))
        }
        when {
            filled -> context.withFilledSR(colour.a != 1F, action)
            else -> context.withLineSR(action)
        }
    }
}