package rprocess.render

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import com.badlogic.gdx.math.Rectangle

data class Picture(val texture: Texture, val bounds: Rectangle, val grayScale: Boolean = false) : Primitive {
    override fun render(context: Context) {
        context.withBatch {
            if (grayScale) {
                shader = grayScaleShader
            }
            draw(texture, bounds.x, bounds.y, bounds.width, bounds.height)
            shader = null
        }
    }

    companion object {
        private val grayScaleShader: ShaderProgram = ShaderProgram(handle("grayscale.vert"), handle("grayscale.frag")).also {
            if (!it.isCompiled) {
                Gdx.app.error("grayscaleShader", "compilation failed:\n" + it.log)
            }
        }
    }
}