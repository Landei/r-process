package rprocess.render

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite as GdxSprite

data class CircularSprite(val texture: Texture, val radius: Float) : Primitive {

    val sprite = GdxSprite(texture)

    init {
        sprite.setSize(2* radius, 2* radius)
        sprite.setCenter(radius, radius)
        sprite.setOriginCenter()
    }

    override fun render(context: Context) {
        context.withBatch {
            sprite.draw(this)
        }
    }
}