package rprocess.render

interface Primitive {
    fun render(context: Context)
}