package rprocess.render.actors

import com.badlogic.gdx.math.Circle
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Touchable

abstract class RoundContextActor : ContextActor() {

    abstract val bounds: Circle

    override fun hit(x: Float, y: Float, touchable: Boolean): Actor? = when {
        touchable && this.touchable != Touchable.enabled -> null
        !isVisible -> null
        bounds.contains(x, y) -> this
        else -> null
    }
}