package rprocess.render.actors

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Rectangle
import rprocess.Assets
import rprocess.render.*

class InfoDialog(title: String,
                 text: String,
                 button: String,
                 bounds: Rectangle) : ModalContextActor() {
    private val primitives: Array<Primitive>

    init {
        val (titleWidth, titleHeight) = Assets.mainFont.size(title, 0.5F)
        val lineHeight = bounds.y + bounds.height - titleHeight - 40F

        val w2 = bounds.width / 2F

        primitives = arrayOf(
                Picture(Assets.dialog, bounds),
                Text(title, at(bounds.x + w2 - titleWidth / 2F, bounds.y + bounds.height - titleHeight), 0.5F, Color.GOLD),
                Text(text, at(bounds.x + 20F, bounds.y + bounds.height - 3.5F * titleHeight), 0.5F, Color.GOLD),
                Rect(bounds, Color.GOLD),
                Line(at(bounds.x, lineHeight), at(bounds.x + bounds.width, lineHeight), Color.GOLD))

        val leftBounds = Rectangle(bounds.x + w2 / 2, bounds.y + 20F, w2, titleHeight + 20F).pad(-20F, 0F)
        addActor(TextButton(button, leftBounds) { it.parent.remove() })
    }

    override fun render() = primitives
}