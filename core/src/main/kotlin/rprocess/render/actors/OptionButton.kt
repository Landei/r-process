package rprocess.render.actors

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Rectangle
import rprocess.Assets
import rprocess.Sounds
import rprocess.render.*
import rprocess.util.Property

class OptionButton<T>(
        caption: String,
        val bounds: Rectangle,
        private val id: T,
        val property: Property<T>) : ContextActor() {

    private val text: Text

    init {
        setBounds(bounds)

        val (textWidth, textHeight) = Assets.mainFont.size(caption, 0.5F)
        text = Text(caption, at(x + width / 2F - textWidth / 2F, y + textHeight + 10F), 0.5F, Color.WHITE)

        addListener(LeftClickListener {
            Sounds.click()
            property(id)
        })
    }

    override fun render(): Array<Primitive> = arrayOf(
            * if (property() == id) arrayOf(Hexagon(x, y, width, height, Color.GOLD)) else arrayOf(),
            text
    )
}