package rprocess.render.actors

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Actor
import rprocess.render.Context
import rprocess.render.Primitive

abstract class ContextActor : Actor() {

    final override fun draw(batch: Batch, parentAlpha: Float) {
        Context(batch).drawAndClose(* render())
    }

    abstract fun render(): Array<Primitive>
}