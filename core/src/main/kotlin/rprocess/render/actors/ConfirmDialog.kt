package rprocess.render.actors

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.Actor
import rprocess.Assets
import rprocess.render.*

class ConfirmDialog(title: String,
                    left: String,
                    right: String,
                    bounds: Rectangle,
                    leftAction: (Actor) -> Unit,
                    rightAction: (Actor) -> Unit) : ModalContextActor() {
    private val primitives: Array<Primitive>

    init {
        val (titleWidth, titleHeight) = Assets.mainFont.size(title, 0.5F)
        val lineHeight = bounds.y + bounds.height - titleHeight - 40F

        val w2 = bounds.width / 2F

        val leftBounds = Rectangle(bounds.x, bounds.y + 20F, w2, titleHeight + 20F).pad(-20F, 0F)

        val rightBounds = Rectangle(bounds.x + w2, bounds.y + 20F, w2, titleHeight + 20F).pad(-20F, 0F)
        primitives = arrayOf(
                Picture(Assets.dialog, bounds),
                Text(title, at(bounds.x + w2 - titleWidth / 2F, bounds.y + bounds.height - titleHeight), 0.5F, Color.GOLD),
                Rect(bounds, Color.GOLD),
                Line(at(bounds.x, lineHeight), at(bounds.x + bounds.width, lineHeight), Color.GOLD))

        addActor(TextButton(left, leftBounds, leftAction))
        addActor(TextButton(right, rightBounds, rightAction))
    }

    override fun render() = primitives
}