package rprocess.render.actors

import rprocess.Main.worldRect
import rprocess.render.setBounds

abstract class ModalContextActor : GroupContextActor() {
    init {
        setBounds(worldRect)
    }
}
