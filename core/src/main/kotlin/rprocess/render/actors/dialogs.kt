package rprocess.render.actors

import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.Stage
import io.vavr.control.Try
import rprocess.Assets
import rprocess.Main
import rprocess.model.Game
import rprocess.render.pad
import rprocess.screen.galaxy.GalaxyScreen
import rprocess.screen.menu.MenuScreen
import rprocess.screen.ship.ShipScreen
import rprocess.screen.starSystem.StarSystemScreen

fun navigationMenu(adapter: ScreenAdapter, lastScreen: (Game) -> ScreenAdapter, stage: Stage, game: () -> Try<Game>) {
    val navBounds = Rectangle(0F, Main.worldHeight - 60F, 60F, 60F)
    stage.addActor(TextureButton(Assets.mainMenuIcon, navBounds.pad(-10F, -10F),
            adapter !is MenuScreen) {
        adapter.dispose()
        Main.screen = MenuScreen(lastScreen, game())
    })
    navBounds.x += 80F
    stage.addActor(TextureButton(Assets.shipIcon, navBounds.pad(-10F, -10F),
            !game().isEmpty && adapter !is ShipScreen) {
        adapter.dispose()
        Main.screen = ShipScreen(game().get())
    })
    navBounds.x += 80F
    stage.addActor(TextureButton(Assets.starSystemIcon, navBounds.pad(-10F, -10F),
            !game().isEmpty && adapter !is StarSystemScreen) {
        adapter.dispose()
        Main.screen = StarSystemScreen(game().get())
    })
    navBounds.x += 80F
    stage.addActor(TextureButton(Assets.galaxyIcon, navBounds.pad(-10F, -10F),
            !game().isEmpty && adapter !is GalaxyScreen) {
        adapter.dispose()
        Main.screen = GalaxyScreen(game().get())
    })
}
