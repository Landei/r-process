package rprocess.render.actors

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.viewport.FitViewport
import com.badlogic.gdx.utils.viewport.StretchViewport
import rprocess.Main
import rprocess.render.Context
import rprocess.render.Context.Companion.sr
import rprocess.render.Primitive
import rprocess.render.Rect

abstract class ContextScreenAdapter : ScreenAdapter() {

    protected val batch = SpriteBatch()
    protected val camera = OrthographicCamera()
    private val gutterViewport = StretchViewport(Main.worldWidth, Main.worldHeight, camera)
    protected val viewport = FitViewport(Main.worldWidth, Main.worldHeight, camera)
    protected val stage = Stage(viewport, batch)

    init {
        sr = ShapeRenderer()
        viewport.apply()

        Gdx.input.inputProcessor = stage

        val coverageSampling = if (Gdx.graphics.bufferFormat.coverageSampling) GL20.GL_COVERAGE_BUFFER_BIT_NV else 0
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT
                or GL20.GL_DEPTH_BUFFER_BIT
                or coverageSampling)

        camera.update()
        sr.projectionMatrix = camera.combined
        batch.projectionMatrix = camera.combined
    }

    override fun render(delta: Float) {
        val context = Context(batch)
        gutterViewport.apply()
        context.drawAndClose(Rect(Main.worldRect, Color.BLACK, true))
        viewport.apply()
        context.drawAndClose(* renderPreStage())
        stage.act()
        stage.draw()
        context.drawAndClose(* renderPostStage())
    }

    open fun renderPreStage(): Array<Primitive> = arrayOf()

    open fun renderPostStage(): Array<Primitive> = arrayOf()

    override fun resize(width: Int, height: Int) {
        gutterViewport.update(width, height)
        viewport.update(width, height)
    }

    override fun dispose() {
        batch.dispose()
        sr.dispose()
        stage.dispose()
        super.dispose()
    }
}