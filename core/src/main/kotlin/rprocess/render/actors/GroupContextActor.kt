package rprocess.render.actors

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Group
import rprocess.render.Context
import rprocess.render.Primitive

abstract class GroupContextActor : Group() {

    abstract fun render(): Array<Primitive>

    final override fun draw(batch: Batch, parentAlpha: Float) {
        Context(batch).drawAndClose(* render())
        super.draw(batch, parentAlpha)
    }

}
