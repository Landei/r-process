package rprocess.render.actors

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.InputListener

class HooverListener(actor: Actor) : InputListener() {

    init{
        actor.addListener(this)
    }

    private var mouseOver = false

    override fun enter(event: InputEvent, x: Float, y: Float, pointer: Int, fromActor: Actor?) {
        mouseOver = true
    }

    override fun exit(event: InputEvent, x: Float, y: Float, pointer: Int, toActor: Actor?) {
        mouseOver = false
    }

    fun hoovering() = mouseOver
}