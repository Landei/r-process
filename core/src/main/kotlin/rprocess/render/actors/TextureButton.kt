package rprocess.render.actors

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.Actor
import rprocess.Sounds
import rprocess.render.*

class TextureButton(
        private val texture: Texture,
        val bounds: Rectangle,
        isTouchable: Boolean,
        action: (Actor) -> Unit) : ContextActor() {

    private val picture: () -> Picture

    init {
        touchable = toTouchable(isTouchable)
        setBounds(bounds)
        addListener(LeftClickListener {
            Sounds.click()
            action.invoke(this)
        })
        val hooverListener = HooverListener(this)

        picture = {
            val picBounds = if (isTouchable && hooverListener.hoovering()) bounds.pad(2F, 2F) else bounds
            Picture(texture, picBounds, !isTouchable)
        }
    }

    override fun render(): Array<Primitive> = arrayOf(picture())
}