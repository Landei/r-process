package rprocess.render.actors

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Touchable
import rprocess.Assets
import rprocess.Sounds
import rprocess.render.*

class TextButton(private val caption: String, bounds: Rectangle, action: (Actor) -> Unit) : ContextActor() {

    private val primitives: () -> Array<Primitive>

    init {
        setBounds(bounds)
        addListener(LeftClickListener {
            Sounds.click()
            action.invoke(this)
        })
        val hooverListener = HooverListener(this)

        val (textWidth, textHeight) = Assets.mainFont.size(caption, 0.5F)
        primitives = {
            val color = when {
                touchable == Touchable.disabled -> Color.SLATE
                hooverListener.hoovering() -> Color.GOLD
                else -> Color.WHITE
            }
            arrayOf(
                    Hexagon(x, y, width, height, color),
                    Text(caption, at(x + width / 2F - textWidth / 2F, y + textHeight + 10F), 0.5F, color)
            )
        }
    }

    override fun render() = primitives()
}