package rprocess.render.actors

import com.badlogic.gdx.Input
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.InputListener

class RightClickListener(private val action: () -> Unit) : InputListener() {
    override fun touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Boolean =
            when (button) {
                Input.Buttons.RIGHT -> true.also { action.invoke() }
                else -> false
            }
}