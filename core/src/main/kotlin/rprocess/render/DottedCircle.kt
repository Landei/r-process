package rprocess.render

import com.badlogic.gdx.graphics.Color
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin

data class DottedCircle(
        val x: Float,
        val y: Float,
        val radius: Float,
        val colour: Color,
        val dotDistance: Float = 5F) : Primitive {

    override fun render(context: Context) {
        context.withPointSR {
            color = colour
            val theta = dotDistance / radius
            for(k in 0.. (2 * PI / theta).toInt()) {
                val phi = k * theta
                point(x + radius * cos(phi), y + radius * sin(phi), 0F)
            }
        }
    }
}