package rprocess.render.layout

import rprocess.render.layout.Rule.Fixed
import rprocess.render.layout.Rule.Proportional
import io.vavr.collection.HashMap as VHashMap
import io.vavr.collection.LinkedHashMap as VLinkedHashMap

sealed class Rule {
    data class Fixed(val amount: Float) : Rule()
    data class Proportional(val percent: Float = 100F) : Rule()
}

fun fixed(amount: Float) = Fixed(amount)

fun prop(percent: Float = 100F) = Proportional(percent)

fun vSplitter(i1: String, r1: Rule) =
        VSplitter(VLinkedHashMap.of(i1, r1))

fun vSplitter(i1: String, r1: Rule, i2: String, r2: Rule) =
        VSplitter(VLinkedHashMap.of(i1, r1, i2, r2))

fun vSplitter(i1: String, r1: Rule, i2: String, r2: Rule, i3: String, r3: Rule) =
        VSplitter(VLinkedHashMap.of(i1, r1, i2, r2, i3, r3))

fun vSplitter(i1: String, r1: Rule, i2: String, r2: Rule, i3: String, r3: Rule, i4: String, r4: Rule) =
        VSplitter(VLinkedHashMap.of(i1, r1, i2, r2, i3, r3, i4, r4))

fun hSplitter(i1: String, r1: Rule) =
        HSplitter(VLinkedHashMap.of(i1, r1))

fun hSplitter(i1: String, r1: Rule, i2: String, r2: Rule) =
        HSplitter(VLinkedHashMap.of(i1, r1, i2, r2))

fun hSplitter(i1: String, r1: Rule, i2: String, r2: Rule, i3: String, r3: Rule) =
        HSplitter(VLinkedHashMap.of(i1, r1, i2, r2, i3, r3))

fun hSplitter(i1: String, r1: Rule, i2: String, r2: Rule, i3: String, r3: Rule, i4: String, r4: Rule) =
        HSplitter(VLinkedHashMap.of(i1, r1, i2, r2, i3, r3, i4, r4))

fun nester(parent: Layouter, i1: String, s1: Layouter) =
        Nester(parent, VHashMap.of(i1, s1))

fun nester(parent: Layouter, i1: String, s1: Layouter, i2: String, s2: Layouter) =
        Nester(parent, VHashMap.of(i1, s1, i2, s2))

fun nester(parent: Layouter, i1: String, s1: Layouter, i2: String, s2: Layouter, i3: String, s3: Layouter) =
        Nester(parent, VHashMap.of(i1, s1, i2, s2, i3, s3))

fun nester(parent: Layouter, i1: String, s1: Layouter, i2: String, s2: Layouter, i3: String, s3: Layouter, i4: String, s4: Layouter) =
        Nester(parent, VHashMap.of(i1, s1, i2, s2, i3, s3, i4, s4))