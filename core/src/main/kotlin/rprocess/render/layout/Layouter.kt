package rprocess.render.layout

import com.badlogic.gdx.math.Rectangle

import io.vavr.collection.Map as VMap

abstract class Layouter {
    abstract fun layout(bounds: Rectangle): VMap<String, Rectangle>

    fun layout(x: Float, y: Float, width: Float, height: Float) = layout(Rectangle(x, y, width, height))
}