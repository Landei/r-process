package rprocess.render.layout

import com.badlogic.gdx.math.Rectangle
import io.vavr.collection.LinkedHashMap
import io.vavr.collection.Map

class HSplitter(val columns: LinkedHashMap<String, Rule>) : Layouter() {

    override fun layout(bounds: Rectangle): Map<String, Rectangle> {
        val remaining = bounds.width - columns.values().map {
            if (it is Rule.Fixed) it.amount else 0F
        }.fold(0F, Float::plus)
        var left = bounds.x
        return columns.mapValues { rule ->
            when (rule) {
                is Rule.Fixed -> Rectangle(left, bounds.y, rule.amount, bounds.height)
                        .also { left += it.width }
                is Rule.Proportional -> Rectangle(left, bounds.y, remaining * rule.percent / 100, bounds.height)
                        .also { left += it.width }
            }
        }
    }
}