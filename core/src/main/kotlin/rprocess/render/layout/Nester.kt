package rprocess.render.layout

import com.badlogic.gdx.math.Rectangle
import io.vavr.kotlin.pair
import io.vavr.collection.Map as VMap

class Nester(val parent: Layouter, val children: VMap<String, Layouter>) : Layouter() {

    override fun layout(bounds: Rectangle): VMap<String, Rectangle> {
        val parentLayout: VMap<String, Rectangle> = parent.layout(bounds)
        val childLayouts: VMap<String, Rectangle> = children.filterKeys { key -> parentLayout.containsKey(key) }
                .flatMap { t ->
                    val (parentKey, childLayouter) = t.pair()
                    val parentBounds = parentLayout[parentKey].get()
                    childLayouter.layout(parentBounds)
                            .mapKeys { childKey -> "$parentKey.$childKey" }
                }.toMap { it }
        return parentLayout.merge(childLayouts)
    }
}