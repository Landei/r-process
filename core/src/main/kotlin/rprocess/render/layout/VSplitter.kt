package rprocess.render.layout

import com.badlogic.gdx.math.Rectangle
import io.vavr.Tuple
import io.vavr.collection.LinkedHashMap
import io.vavr.collection.Map
import io.vavr.kotlin.pair

class VSplitter(val rows: LinkedHashMap<String, Rule>) : Layouter() {

    override fun layout(bounds: Rectangle): Map<String, Rectangle> {
        val remaining = bounds.height - rows.values().map {
            if (it is Rule.Fixed) it.amount else 0F
        }.fold(0F, Float::plus)
        var bottom = bounds.y
        val tuples = rows.toList().reverse().map { tuple ->
            val (key, rule) = tuple.pair()
            val rect = when (rule) {
                is Rule.Fixed -> Rectangle(bounds.x, bottom, bounds.width, rule.amount)
                        .also { bottom += it.height }
                is Rule.Proportional -> Rectangle(bounds.x, bottom, bounds.width, remaining * rule.percent / 100)
                        .also { bottom += it.height }
            }
            Tuple.of(key, rect)
        }.reverse()
        return LinkedHashMap.ofAll(tuples.toJavaStream(), { it._1 }, { it._2 })
    }
}