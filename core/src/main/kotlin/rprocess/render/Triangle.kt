package rprocess.render

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Vector2

data class Triangle(val p1: Vector2, val p2: Vector2, val p3: Vector2,
                    val colour: Color, val filled: Boolean = false) : Primitive {

    override fun render(context: Context) {
        val action = { sr: ShapeRenderer ->
            sr.color = colour
            sr.triangle(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y)
        }
        when {
            filled -> context.withFilledSR(colour.a != 1F, action)
            else -> context.withLineSR(action)
        }
    }
}