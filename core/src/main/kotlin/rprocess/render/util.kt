package rprocess.render

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Touchable
import rprocess.Main

const val cellWidth: Float = 80F
const val cellHeight: Float = 80F

fun handle(name: String): FileHandle = Gdx.files.internal("core/assets/$name")

fun Actor.setBounds(r: Rectangle) {
    setBounds(r.x, r.y, r.width, r.height)
}

fun Rectangle.pad(dx: Float, dy: Float) = Rectangle(
        this.x - dx,
        this.y - dy,
        this.width + 2 * dx,
        this.height + 2 * dy)

fun Rectangle.center(inside: Rectangle = Main.worldRect) = Rectangle(
        inside.x + (inside.width - width) / 2F,
        inside.y + (inside.height - height) / 2F,
        width,
        height)

fun BitmapFont.size(text: String, scale: Float): Pair<Float, Float> {
    this.data.setScale(scale)
    return GlyphLayout().let { layout ->
        layout.setText(this, text)
        Pair(layout.width, layout.height)
    }
}

fun at(x: Float, y: Float) = Vector2(x, y)

fun toTouchable(test: Boolean) = if (test) Touchable.enabled else Touchable.disabled