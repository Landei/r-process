package rprocess.render

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.glutils.ShapeRenderer

data class Circle(val x: Float, val y: Float, val radius: Float, val colour: Color, val filled: Boolean = false) : Primitive {

    override fun render(context: Context) {
        val action = { sr: ShapeRenderer ->
            sr.color = colour
            sr.circle(x, y, radius)
        }
        when {
            filled -> context.withFilledSR(colour.a != 1F, action)
            else -> context.withLineSR(action)
        }
    }
}