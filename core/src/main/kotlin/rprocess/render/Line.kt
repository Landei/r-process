package rprocess.render

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Vector2

data class Line(val from: Vector2, val to: Vector2, val colour: Color) : Primitive {
    override fun render(context: Context) {
        context.withLineSR {
            color = colour
            line(from, to)
        }
    }
}