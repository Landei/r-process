package rprocess.render

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.graphics.g2d.Sprite as GdxSprite

data class Sprite(val sprite: GdxSprite, val at: Vector2, val grayScale: Boolean = false) : Primitive {
    override fun render(context: Context) {
        context.withBatch {
            if (grayScale) {
                shader = grayScaleShader
            }
            sprite.setPosition(at.x, at.y)
            sprite.draw(this)
            shader = null
        }
    }

    companion object {
        private val grayScaleShader: ShaderProgram = ShaderProgram(handle("grayscale.vert"), handle("grayscale.frag")).also {
            if (!it.isCompiled) {
                Gdx.app.error("grayscaleShader", "compilation failed:\n" + it.log)
            }
        }
    }
}