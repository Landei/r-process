package rprocess.util

abstract class Property<A> {
    abstract operator fun invoke(): A
    abstract operator fun invoke(a: A)
    fun modify(f: (A) -> A) = invoke(f(invoke()))
}