package rprocess.util

import java.util.*

fun sqr(x: Float) = x * x

fun sqr(x: Int) = x * x

fun <T> Array<T>.random(rng: Random) = this[rng.nextInt(this.size)]