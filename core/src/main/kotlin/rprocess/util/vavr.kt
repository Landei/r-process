package rprocess.util

import io.vavr.Tuple2
import io.vavr.collection.Seq
import io.vavr.collection.Set

inline fun <reified T> Seq<T>.toTypedArray(): Array<T> = this.toJavaList().toTypedArray()
inline fun <reified T> Set<T>.toTypedArray(): Array<T> = this.toJavaSet().toTypedArray()

fun <A, B, C> Tuple2<Tuple2<A, B>, C>.triple() = Triple(this._1._1, this._1._2, this._2)