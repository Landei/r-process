package rprocess.util

import rprocess.Settings

fun String.i18n(): String = Settings.i18n(this)

fun String.i18n(vararg args: Any) = String.format(i18n(), *args)
