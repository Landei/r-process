package rprocess.util

import io.vavr.collection.List as VList
import io.vavr.collection.Map as VMap

fun <E> listL(index: Int) = Lens<VList<E>, E>(
        { list -> list.get(index) },
        { list, value -> list.removeAt(index).insert(index, value) })

//of course, pred must identify exactly one element in the list
fun <E> listL(pred: (E) -> Boolean) = Lens<VList<E>, E> (
        { list -> list.first(pred)},
        { list, value -> list.map { e -> if (pred(e)) value else e }}
)

fun <K, V> mapL(key: K) = Lens<VMap<K, V>, V>(
        { map -> map.get(key).get() },
        { map, value -> map.put(key, value) })
