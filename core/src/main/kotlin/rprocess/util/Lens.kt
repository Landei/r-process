package rprocess.util

class Lens<A, B>(private val getter: (A) -> B, private val setter: (A, B) -> A) {

    operator fun invoke(target: A): B = getter.invoke(target)

    fun set(target: A, value: B): A = setter.invoke(target, value)

    fun modify(target: A, mapper: (B) -> B): A = set(target, mapper.invoke(this(target)))

    operator fun <C> times(other: Lens<B, C>): Lens<A, C> = Lens(
            { a: A -> other(this(a)) },
            { a: A, c: C -> set(a, other.set(this(a), c)) }
    )
}