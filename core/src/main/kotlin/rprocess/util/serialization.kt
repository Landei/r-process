package rprocess.util

import com.google.gson.*
import com.google.gson.reflect.TypeToken
import io.vavr.Tuple2
import io.vavr.gson.VavrGson
import rprocess.model.Game
import rprocess.model.Pos
import rprocess.model.galaxy.Coord
import rprocess.model.galaxy.Galaxy
import rprocess.model.galaxy.Route
import rprocess.model.galaxy.Star
import rprocess.model.history.History
import rprocess.model.resources.Deposit
import rprocess.model.resources.Immaterial
import rprocess.model.resources.Resource
import rprocess.model.ship.*
import java.lang.reflect.Type
import io.vavr.collection.HashMap as VHashMap
import io.vavr.collection.HashSet as VHashSet
import io.vavr.collection.Map as VMap

val gson: Gson = GsonBuilder().also {
    VavrGson.registerAll(it)
    val depositsType = object : TypeToken<VMap<Pos, Deposit>>() {}.type
    it.registerTypeAdapter(depositsType, DepositsTypeAdapter)
    it.registerTypeAdapter(Cell::class.java, CellTypeAdapter)
}.create()

object DepositsTypeAdapter :
        JsonSerializer<VMap<Pos, Deposit>>,
        JsonDeserializer<VMap<Pos, Deposit>> {

    private val listType = object : TypeToken<List<Pair<Pos, Deposit>>>() {}.type

    override fun serialize(src: VMap<Pos, Deposit>, typeOfSrc: Type, context: JsonSerializationContext): JsonElement {
        val list: List<Pair<Pos, Deposit>> = src.map { t -> t._1 to t._2 }.toJavaList()
        return context.serialize(list, listType)
    }

    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): VMap<Pos, Deposit> {
        val list: MutableList<Pair<Pos, Deposit>> = mutableListOf()
        json.asJsonArray.forEach { elem ->
            val pair = elem.asJsonObject
            val pos = context.deserialize<Pos>(pair.get("first"), Pos::class.java)
            val deposit = context.deserialize<Deposit>(pair.get("second"), Deposit::class.java)
            list.add(pos to deposit)
        }
        return VHashMap.ofAll(list.stream()){ pair -> Tuple2(pair.first, pair.second) }
    }
}

object CellTypeAdapter :
        JsonSerializer<Cell>,
        JsonDeserializer<Cell> {
    override fun serialize(src: Cell, typeOfSrc: Type, context: JsonSerializationContext): JsonElement =
        JsonObject().also {
            it.add("resource", JsonPrimitive(when (src) {
                is EmptyCell -> "EMPTY_CELL"
                is DamagedCell -> "DAMAGED_CELL"
                is Cargo -> src.resource.name
            }))
            it.add("amount", JsonPrimitive(src.amount))
        }

    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): Cell {
        with(json.asJsonObject) {
            val resource = getAsJsonPrimitive("resource").asString
            val amount = getAsJsonPrimitive("amount").asInt
            return when {
                resource == "EMPTY_CELL" -> EmptyCell
                resource == "DAMAGED_CELL" -> DamagedCell
                else -> Cargo(Resource.valueOf(resource), amount)
            }
        }
    }
}

data class GalaxyJson(val systems: List<Pair<Coord, Star>>, val routes: List<Route>) {

    constructor(galaxy: Galaxy) : this(
            galaxy.systems.map { t -> t._1 to t._2 }.toJavaList(),
            galaxy.routes.toJavaList())

    fun toGalaxy() = Galaxy(
            VHashMap.ofAll(systems.stream()) { pair -> Tuple2(pair.first, pair.second) },
            VHashSet.ofAll(routes.stream())
    )
}

data class ShipJson(val shipType: ShipType,
                    val name: String,
                    val cells: List<Pair<Pos, Cell>>,
                    val immaterial: List<Immaterial>) {
    constructor(ship: Ship) : this(
            ship.type,
            ship.name,
            ship.cells.map { t -> t._1 to t._2 }.toJavaList(),
            ship.immaterials.toJavaList())

    fun toShip() = Ship(
            shipType,
            name,
            VHashMap.ofAll(cells.stream()) { pair -> Tuple2(pair.first, pair.second) },
            VHashSet.ofAll(immaterial))
}

data class GameJson(val ship: ShipJson,
                    val galaxy: GalaxyJson,
                    val history: History) {
    constructor(game: Game) : this(
            ShipJson(game.ship),
            GalaxyJson(game.galaxy),
            game.history)

    fun toGame() = Game(
            ship.toShip(),
            galaxy.toGalaxy(),
            history)
}
