package rprocess.render.layout

import com.badlogic.gdx.math.Rectangle
import io.vavr.Tuple
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class HSplitterTest {

    @Test
    fun fixedSplitter() {
        val layout = hSplitter(
                "left", fixed(10F),
                "middle", fixed(20F),
                "right", fixed(30F)
        ).layout(11F, 22F, 60F, 50F)
        assertThat(layout).containsExactlyInAnyOrder(
                Tuple.of("left", Rectangle(11F, 22F, 10F, 50F)),
                Tuple.of("middle", Rectangle(11F + 10F, 22F, 20F, 50F)),
                Tuple.of("right", Rectangle(11F + 10F + 20F, 22F, 30F, 50F))
        )
    }

    @Test
    fun propSplitter() {
        val layout = hSplitter(
                "left", prop(20F), //10px
                "middle", prop(30F), //15px
                "right", prop(50F) //25px
        ).layout(11F, 22F, 50F, 50F)
        assertThat(layout).containsExactlyInAnyOrder(
                Tuple.of("left", Rectangle(11F, 22F, 10F, 50F)),
                Tuple.of("middle", Rectangle(11F + 10F, 22F, 15F, 50F)),
                Tuple.of("right", Rectangle(11F + 10F + 15F, 22F, 25F, 50F))
        )
    }

    @Test
    fun mixedSplitter() {
        val layout = hSplitter(
                "left", prop(20F), //10px
                "lefty", fixed(20F),
                "righty", fixed(30F),
                "right", prop(80F) //40px
        ).layout(11F, 22F, 100F, 50F)
        assertThat(layout).containsExactlyInAnyOrder(
                Tuple.of("left", Rectangle(11F, 22F, 10F, 50F)),
                Tuple.of("lefty", Rectangle(11F + 10F, 22F, 20F, 50F)),
                Tuple.of("righty", Rectangle(11F + 10F + 20F, 22F, 30F, 50F)),
                Tuple.of("right", Rectangle(11F + 10F + 20F + 30F, 22F, 40F, 50F))
        )
    }

}