package rprocess.render.layout

import com.badlogic.gdx.math.Rectangle
import io.vavr.Tuple
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class VSplitterTest {

    @Test
    fun fixedSplitter() {
        val layout = vSplitter(
                "top", fixed(10F),
                "middle", fixed(20F),
                "bottom", fixed(30F)
        ).layout(11F, 22F, 50F, 60F)
        assertThat(layout).containsExactlyInAnyOrder(
                Tuple.of("top", Rectangle(11F, 22F + 30F + 20F, 50F, 10F)),
                Tuple.of("middle", Rectangle(11F, 22F + 30F, 50F, 20F)),
                Tuple.of("bottom", Rectangle(11F, 22F + 0F, 50F, 30F))
        )
    }

    @Test
    fun propSplitter() {
        val layout = vSplitter(
                "top", prop(20F), //10px
                "middle", prop(30F), //15px
                "bottom", prop(50F) //25px
        ).layout(11F, 22F, 50F, 50F)
        assertThat(layout).containsExactlyInAnyOrder(
                Tuple.of("top", Rectangle(11F, 22F + 25F + 15F, 50F, 10F)),
                Tuple.of("middle", Rectangle(11F, 22F + 25F, 50F, 15F)),
                Tuple.of("bottom", Rectangle(11F, 22F + 0F, 50F, 25F))
        )
    }

    @Test
    fun mixedSplitter() {
        val layout = vSplitter(
                "top", prop(20F), //10px
                "upper", fixed(20F),
                "lower", fixed(30F),
                "bottom", prop(80F) //40px
        ).layout(11F, 22F, 50F, 100F)
        assertThat(layout).containsExactlyInAnyOrder(
                Tuple.of("top", Rectangle(11F, 22F + 40F + 30F + 20F, 50F, 10F)),
                Tuple.of("upper", Rectangle(11F, 22F + 40F + 30F, 50F, 20F)),
                Tuple.of("lower", Rectangle(11F, 22F + 40F, 50F, 30F)),
                Tuple.of("bottom", Rectangle(11F, 22F + 0F, 50F, 40F))
        )
    }

}