package rprocess.render.layout

import com.badlogic.gdx.math.Rectangle
import io.vavr.Tuple
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class NesterTest {

    @Test
    fun nested() {
        val vSplitter = vSplitter("top", prop(40F), "bottom", prop(60F))
        val hSplitter = hSplitter("left", prop(30F), "right", prop(70F))
        val nester = nester(vSplitter, "bottom", hSplitter)

        assertThat(nester.layout(11F, 22F, 100F, 100F)).containsExactlyInAnyOrder(
                Tuple.of("top", Rectangle(11F, 22F + 60F , 100F, 40F)),
                Tuple.of("bottom", Rectangle(11F, 22F , 100F, 60F)),
                Tuple.of("bottom.left", Rectangle(11F, 22F , 30F, 60F)),
                Tuple.of("bottom.right", Rectangle(11F + 30F, 22F , 70F, 60F))
        )
    }
}