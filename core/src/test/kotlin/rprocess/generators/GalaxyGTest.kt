package rprocess.generators

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import rprocess.model.galaxy.Coord

import io.vavr.collection.List as VList

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class GalaxyGTest {

    @Test
    fun makeRoutes_singleRoute() {
        val c1 = Coord(0, 0)
        val c2 = Coord(0, 7)
        val routes = GalaxyG.makeRoutes(VList.of(c1, c2))
        assertThat(routes).hasSize(1)
        assertThat(routes.get().between(c1, c2))
    }

    @Test
    fun makeRoutes_crossing() {
        val c1 = Coord(0, 0)
        val c2 = Coord(0, 7)
        val c3 = Coord(6, 0)
        val c4 = Coord(6, 6)
        val routes = GalaxyG.makeRoutes(VList.of(c1, c2, c3, c4))
        assertThat(routes).hasSize(5)
        assertThat(routes).noneMatch { route -> route.between(c2, c3) }
    }

    @Test
    fun makeRoutes_notCrossing() {
        val c1 = Coord(0, 0)
        val c2 = Coord(0, 7)
        val c3 = Coord(1, 10)
        val c4 = Coord(0, 18)
        val routes = GalaxyG.makeRoutes(VList.of(c1, c2, c3, c4))
        assertThat(routes).hasSize(3)
    }

}