package rprocess.util

import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import rprocess.Settings
import rprocess.model.resources.Immaterial
import rprocess.model.resources.Recipe
import rprocess.model.resources.Resource
import java.util.*

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class I18NTest {

    @BeforeAll
    internal fun beforeAll() {
        Settings.locale = Locale.ENGLISH
    }

    @Test
    fun recipes() {
        Recipe.values().map { it.description() }
    }

    @Test
    fun resources() {
        Resource.values().map { it.description() }
    }
    
    @Test
    fun immaterial() {
        Immaterial.values().map { it.description() }
    }
}