package rprocess.util

import io.vavr.Tuple2
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import rprocess.model.galaxy.Moon
import rprocess.model.galaxy.MoonType
import rprocess.model.galaxy.Planet
import rprocess.model.galaxy.PlanetType
import rprocess.model.ship.Ship
import rprocess.model.ship.ShipType
import io.vavr.collection.HashMap as VHashMap
import io.vavr.collection.HashSet as VHashSet
import io.vavr.collection.List as VList

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class LensTest {

    @Test
    fun listL() {
        val list = VList.of("foo", "bar", "baz")

        assertThat(listL<String>(0)(list)).isEqualTo("foo")
        assertThat(listL<String>(1)(list)).isEqualTo("bar")
        assertThat(listL<String>(2)(list)).isEqualTo("baz")

        assertThat(listL<String>(0).set(list, "quux")).containsExactly("quux", "bar", "baz")
        assertThat(listL<String>(1).set(list, "quux")).containsExactly("foo", "quux", "baz")
        assertThat(listL<String>(2).set(list, "quux")).containsExactly("foo", "bar", "quux")
    }

    @Test
    fun listL_pred() {
        val list = VList.of("one", "two", "three")

        assertThat(listL<String> { it.startsWith("o") }(list)).isEqualTo("one")
        assertThat(listL<String> { it.startsWith("tw") }(list)).isEqualTo("two")
        assertThat(listL<String> { it.startsWith("th") }(list)).isEqualTo("three")

        assertThat(listL<String> { it.startsWith("o") }.set(list, "five"))
                .containsExactly("five", "two", "three")
        assertThat(listL<String> { it.startsWith("tw") }.set(list, "five"))
                .containsExactly("one", "five", "three")
        assertThat(listL<String> { it.startsWith("th") }.set(list, "five"))
                .containsExactly("one", "two", "five")
    }

    @Test
    fun mapL() {
        val map = VHashMap.of(
                "foo", 1.0,
                "bar", 2.0,
                "baz", 3.0)

        assertThat(mapL<String, Double>("foo")(map)).isEqualTo(1.0)
        assertThat(mapL<String, Double>("bar")(map)).isEqualTo(2.0)
        assertThat(mapL<String, Double>("baz")(map)).isEqualTo(3.0)

        assertThat(mapL<String, Double>("foo").set(map, 4.0)).contains(
                Tuple2("foo", 4.0), Tuple2("bar", 2.0), Tuple2("baz", 3.0))
        assertThat(mapL<String, Double>("bar").set(map, 4.0)).contains(
                Tuple2("foo", 1.0), Tuple2("bar", 4.0), Tuple2("baz", 3.0))
        assertThat(mapL<String, Double>("baz").set(map, 4.0)).contains(
                Tuple2("foo", 1.0), Tuple2("bar", 2.0), Tuple2("baz", 4.0))
    }

    @Test
    fun composition() {
        val moon1 = Moon("moon1", 23F, VHashMap.empty(), 100F, MoonType.MAGMA)
        val moon2 = Moon("moon2", 23F, VHashMap.empty(), 100F, MoonType.MAGMA)
        val planet = Planet("planet", 50F, VHashMap.empty(), 400F, PlanetType.HOT,
                VList.of(moon1, moon2))

        val lens = Planet.moonsL * listL(1) * Moon.nameL

        assertThat(lens(planet)).isEqualTo("moon2")

        val modifiedPlanet = lens.set(planet, "io")
        assertThat(modifiedPlanet.moons.get(0).name).isEqualTo("moon1")
        assertThat(modifiedPlanet.moons.get(1).name).isEqualTo("io")
    }

    @Test
    fun modify() {
        val ship = Ship(ShipType.FREIGHTER, "test", VHashMap.empty(), VHashSet.empty())
        val modifiedShip = Ship.nameL.modify(ship, String::toUpperCase)
        assertThat(modifiedShip.name).isEqualTo("TEST")
    }
}