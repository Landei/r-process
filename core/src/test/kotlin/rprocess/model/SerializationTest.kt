package rprocess.model

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import rprocess.model.galaxy.*
import rprocess.model.history.History
import rprocess.model.resources.Deposit
import rprocess.model.resources.Immaterial
import rprocess.model.resources.Resource
import rprocess.model.ship.Cargo
import rprocess.model.ship.EmptyCell
import rprocess.model.ship.Ship
import rprocess.model.ship.ShipType
import rprocess.util.GalaxyJson
import rprocess.util.GameJson
import rprocess.util.ShipJson
import rprocess.util.gson
import io.vavr.collection.HashMap as VHashMap
import io.vavr.collection.HashSet as VHashSet
import io.vavr.collection.List as VList

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class SerializationTest {

    @Test
    fun coordRoundTrip() {
        val original = Coord(47, 11)
        val json = gson.toJson(original)
        val copy = gson.fromJson(json, Coord::class.java)
        assertThat(copy).isEqualTo(original)
    }

    @Test
    fun posRoundTrip() {
        val original = Pos(47, 11)
        val json = gson.toJson(original)
        val copy = gson.fromJson(json, Pos::class.java)
        assertThat(copy).isEqualTo(original)
    }

    @Test
    fun routeRoundTrip() {
        val original = Route(Coord(47, 11), Coord(8, 15))
        val json = gson.toJson(original)
        val copy = gson.fromJson(json, Route::class.java)
        assertThat(copy).isEqualTo(original)
    }

    @Test
    fun resourceRoundTrip() {
        val original = Resource.CARBOHYDRATES
        val json = gson.toJson(original)
        val copy = gson.fromJson(json, Resource::class.java)
        assertThat(copy).isEqualTo(original)
    }

    @Test
    fun depositRoundTrip() {
        val original = Deposit(Resource.HELIUM, 5, false)
        val json = gson.toJson(original)
        val copy = gson.fromJson(json, Deposit::class.java)
        assertThat(copy).isEqualTo(original)
    }

    @Test
    fun moonRoundTrip() {
        val deposits = VHashMap.of(
                Pos(0,0), Deposit(Resource.URANIUM, 5),
                Pos(0,1), Deposit(Resource.QUARTZ, 15)
        )
        val original = Moon("Hugo a I", 7.11F, deposits, 23F, MoonType.MAGMA)
        val json = gson.toJson(original)
        val copy = gson.fromJson(json, Moon::class.java)
        assertThat(copy).isEqualTo(original)
    }

    @Test
    fun planetRoundTrip() {
        val deposits1 = VHashMap.of(
                Pos(0,0), Deposit(Resource.URANIUM, 5),
                Pos(0,1), Deposit(Resource.QUARTZ, 15)
        )
        val deposits2 = VHashMap.of(
                Pos(0,1), Deposit(Resource.URANIUM, 5),
                Pos(0,2), Deposit(Resource.QUARTZ, 15)
        )
        val moons = VList.of(
                Moon("Hugo a I", 7.11F, deposits1, 23F, MoonType.MAGMA),
                Moon("Hugo a II", 7.11F, deposits2, 25F, MoonType.MAGMA)
        )

        val deposits = VHashMap.of(
                Pos(0,0), Deposit(Resource.WATER, 5),
                Pos(0,1), Deposit(Resource.QUARTZ, 15)
        )

        val original = Planet("Hugo a", 27.11F, deposits, 123F, PlanetType.DESERT, moons)
        val json = gson.toJson(original)
        val copy = gson.fromJson(json, Planet::class.java)
        assertThat(copy).isEqualTo(original)
    }

    @Test
    fun starRoundTrip() {
        val depositsM1 = VHashMap.of(
                Pos(0,0), Deposit(Resource.URANIUM, 5),
                Pos(0,1), Deposit(Resource.QUARTZ, 15)
        )
        val depositsM2 = VHashMap.of(
                Pos(0,0), Deposit(Resource.WATER, 5)
        )
        val moons = VList.of(
                Moon("Hugo a I", 7.11F, depositsM1, 23F, MoonType.MAGMA),
                Moon("Hugo a II", 7.11F, depositsM2, 25F, MoonType.MAGMA)
        )

        val depositsP1 = VHashMap.of(
                Pos(0,0), Deposit(Resource.WATER, 5),
                Pos(0,1), Deposit(Resource.QUARTZ, 15)
        )
        val depositsP2 = VHashMap.of(
                Pos(0,2), Deposit(Resource.QUARTZ, 15)
        )
        val planets = VList.of(
                Planet("Hugo a", 27.11F, depositsP1, 123F, PlanetType.DESERT, moons),
                Planet("Hugo b", 37.11F, depositsP2, 234F, PlanetType.GAS_GIANT, VList.of())
        )
        val depositsS = VHashMap.of(
                Pos(0,0), Deposit(Resource.HELIUM, 5),
                Pos(0,1), Deposit(Resource.HYDROGEN, 15)
        )
        val original = Star("Hugo", 47.11F, depositsS, StarType.BLUE, planets)
        val json = gson.toJson(original)
        val copy = gson.fromJson(json, Star::class.java)
        assertThat(copy).isEqualTo(original)
    }

    @Test
    fun galaxyRoundTrip() {
        val depositsP1 = VHashMap.of(
                Pos(0,0), Deposit(Resource.WATER, 5),
                Pos(0,1), Deposit(Resource.QUARTZ, 15)
        )
        val planets = VList.of(
                Planet("Reusable", 37.11F, depositsP1, 234F, PlanetType.GAS_GIANT, VList.of())
        )
        val depositsS = VHashMap.of(
                Pos(0,0), Deposit(Resource.HELIUM, 5),
                Pos(0,1), Deposit(Resource.HYDROGEN, 15)
        )
        val stars = VHashMap.of(
                Coord(47, 11), Star("Hugo", 47.11F, depositsS, StarType.BLUE, planets),
                Coord(8, 15), Star("Coco", 57.11F, depositsS, StarType.RED, planets),
                Coord(-3, -7), Star("Louis", 55.55F, depositsS, StarType.BROWN, planets)
        )

        val routes = VHashSet.of(
                Route(Coord(47, 11), Coord(8, 15)),
                Route(Coord(-3, -7), Coord(8, 15))
        )

        val original = Galaxy(stars, routes)
        val json = gson.toJson(GalaxyJson(original))
        val copy = gson.fromJson(json, GalaxyJson::class.java).toGalaxy()
        assertThat(copy).isEqualTo(original)
    }

    @Test
    fun cargoRoundTrip() {
        val original = Cargo(Resource.HELIUM, 11)
        val json = gson.toJson(original)
        val copy = gson.fromJson(json, Cargo::class.java)
        assertThat(copy).isEqualTo(original)
    }

    @Test
    fun shipRoundTrip() {
        val cells = VHashMap.of(Pos(0, 1), Cargo(Resource.HELIUM, 12), Pos(0, 0), EmptyCell)
        val immaterial = VHashSet.of(Immaterial.ELECTROLYSIS)
        val original = Ship(ShipType.FREIGHTER, "Cthulhu", cells, immaterial)
        val json = gson.toJson(ShipJson(original))
        val copy = gson.fromJson(json, ShipJson::class.java).toShip()
        assertThat(copy).isEqualTo(original)
    }

    @Test
    fun gameRoundTrip() {
        val cells = VHashMap.of(Pos(0, 1), Cargo(Resource.HELIUM, 12), Pos(0, 0), EmptyCell)
        val immaterial = VHashSet.of(Immaterial.ELECTROLYSIS)
        val ship = Ship(ShipType.FREIGHTER, "Cthulhu", cells, immaterial)

        val depositsP1 = VHashMap.of(
                Pos(0,0), Deposit(Resource.WATER, 5),
                Pos(0,1), Deposit(Resource.QUARTZ, 15)
        )
        val planets = VList.of(
                Planet("Reusable", 37.11F, depositsP1, 234F, PlanetType.GAS_GIANT, VList.of())
        )
        val depositsS = VHashMap.of(
                Pos(0,0), Deposit(Resource.HELIUM, 5),
                Pos(0,1), Deposit(Resource.HYDROGEN, 15)
        )
        val stars = VHashMap.of(
                Coord(47, 11), Star("Hugo", 47.11F, depositsS, StarType.BLUE, planets),
                Coord(8, 15), Star("Coco", 57.11F, depositsS, StarType.RED, planets),
                Coord(-3, -7), Star("Louis", 55.55F, depositsS, StarType.BROWN, planets)
        )

        val routes = VHashSet.of(
                Route(Coord(47, 11), Coord(8, 15)),
                Route(Coord(-3, -7), Coord(8, 15))
        )

        val galaxy = Galaxy(stars, routes)
        val original = Game(ship, galaxy, History(VList.of(Coord(8, 15)),10L))
        val json = gson.toJson(GameJson(original))
        val copy = gson.fromJson(json, GameJson::class.java).toGame()
        assertThat(copy).isEqualTo(original)
    }

}