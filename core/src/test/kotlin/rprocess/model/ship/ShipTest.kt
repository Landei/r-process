package rprocess.model.ship

import io.vavr.Tuple
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import rprocess.model.Pos
import rprocess.model.galaxy.Star
import rprocess.model.galaxy.StarType
import rprocess.model.resources.Immaterial
import rprocess.model.resources.Resource
import io.vavr.collection.HashMap as VHashMap
import io.vavr.collection.HashSet as VHashSet
import io.vavr.collection.List as VList

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class ShipTest {

    @Test
    fun inventory() {
        val cells = VHashMap.of(
                Pos(0, 0), Cargo(Resource.URANIUM, 3),
                Pos(0, 1), Cargo(Resource.HELIUM, 3),
                Pos(1, 1), Cargo(Resource.HELIUM, 5),
                Pos(1, 0), EmptyCell
        )
        val immaterial = VHashSet.of(Immaterial.ELECTROLYSIS)
        val ship = Ship(ShipType.FREIGHTER, "titanic", cells, immaterial)

        assertThat(ship.inventory()).containsExactlyInAnyOrder(
                Tuple.of(Resource.URANIUM, 3),
                Tuple.of(Resource.HELIUM, 8),
                Tuple.of(Immaterial.ELECTROLYSIS, 1)
        )
    }

    @Test
    fun removeResource() {
        val cells = VHashMap.of(
                Pos(0, 0), Cargo(Resource.URANIUM, 3),
                Pos(0, 1), Cargo(Resource.HELIUM, 3),
                Pos(1, 1), Cargo(Resource.HELIUM, 5),
                Pos(1, 0), EmptyCell
        )
        val immaterial = VHashSet.of(Immaterial.ELECTROLYSIS)
        val ship = Ship(ShipType.FREIGHTER, "titanic", cells, immaterial)

        assertThat(ship.removeResource(Resource.URANIUM, 1).get().cells).containsExactlyInAnyOrder(
                Tuple.of(Pos(0, 0), Cargo(Resource.URANIUM, 2)),
                Tuple.of(Pos(0, 1), Cargo(Resource.HELIUM, 3)),
                Tuple.of(Pos(1, 1), Cargo(Resource.HELIUM, 5)),
                Tuple.of(Pos(1, 0), EmptyCell)
        )

        assertThat(ship.removeResource(Resource.URANIUM, 3).get().cells).containsExactlyInAnyOrder(
                Tuple.of(Pos(0, 1), Cargo(Resource.HELIUM, 3)),
                Tuple.of(Pos(1, 1), Cargo(Resource.HELIUM, 5)),
                Tuple.of(Pos(0, 0), EmptyCell),
                Tuple.of(Pos(1, 0), EmptyCell)
        )

        assertThat(ship.removeResource(Resource.URANIUM, 4)).isEmpty()

        assertThat(ship.removeResource(Resource.HELIUM, 1).get().cells).containsExactlyInAnyOrder(
                Tuple.of(Pos(0, 0), Cargo(Resource.URANIUM, 3)),
                Tuple.of(Pos(0, 1), Cargo(Resource.HELIUM, 2)),
                Tuple.of(Pos(1, 1), Cargo(Resource.HELIUM, 5)),
                Tuple.of(Pos(1, 0), EmptyCell)
        )

        assertThat(ship.removeResource(Resource.HELIUM, 6).get().cells).containsExactlyInAnyOrder(
                Tuple.of(Pos(0, 0), Cargo(Resource.URANIUM, 3)),
                Tuple.of(Pos(1, 1), Cargo(Resource.HELIUM, 2)),
                Tuple.of(Pos(0, 1), EmptyCell),
                Tuple.of(Pos(1, 0), EmptyCell)
        )

        assertThat(ship.removeResource(Resource.HELIUM, 8).get().cells).containsExactlyInAnyOrder(
                Tuple.of(Pos(0, 0), Cargo(Resource.URANIUM, 3)),
                Tuple.of(Pos(0, 1), EmptyCell),
                Tuple.of(Pos(1, 0), EmptyCell),
                Tuple.of(Pos(1, 1), EmptyCell)
        )

        assertThat(ship.removeResource(Resource.HELIUM, 9)).isEmpty()
    }

    @Test
    fun addResource() {
        val cells = VHashMap.of(
                Pos(0, 0), Cargo(Resource.URANIUM, 3),
                Pos(0, 1), Cargo(Resource.HELIUM, 3),
                Pos(1, 1), Cargo(Resource.HELIUM, 5),
                Pos(1, 0), EmptyCell
        )
        val immaterial = VHashSet.of(Immaterial.ELECTROLYSIS)
        val ship = Ship(ShipType.FREIGHTER, "titanic", cells, immaterial)

        assertThat(ship.addResource(Resource.URANIUM, 38)).isEmpty()

        assertThat(ship.addResource(Resource.URANIUM, 37).get().cells).containsExactlyInAnyOrder(
                Tuple.of(Pos(0, 0), Cargo(Resource.URANIUM, 20)),
                Tuple.of(Pos(1, 0), Cargo(Resource.URANIUM, 20)),
                Tuple.of(Pos(0, 1), Cargo(Resource.HELIUM, 3)),
                Tuple.of(Pos(1, 1), Cargo(Resource.HELIUM, 5))
        )

        assertThat(ship.addResource(Resource.HELIUM, 3).get().cells).containsExactlyInAnyOrder(
                Tuple.of(Pos(0, 0), Cargo(Resource.URANIUM, 3)),
                Tuple.of(Pos(0, 1), Cargo(Resource.HELIUM, 3)),
                Tuple.of(Pos(1, 1), Cargo(Resource.HELIUM, 8)),
                Tuple.of(Pos(1, 0), EmptyCell)
        )

        assertThat(ship.addResource(Resource.HELIUM, 16).get().cells).containsExactlyInAnyOrder(
                Tuple.of(Pos(0, 0), Cargo(Resource.URANIUM, 3)),
                Tuple.of(Pos(0, 1), Cargo(Resource.HELIUM, 4)),
                Tuple.of(Pos(1, 1), Cargo(Resource.HELIUM, 20)),
                Tuple.of(Pos(1, 0), EmptyCell)
        )
    }

    @Test
    fun canScan() {
        val cells = VHashMap.of(
                Pos(0, 0), Cargo(Resource.URANIUM, 3),
                Pos(0, 1), Cargo(Resource.HELIUM, 3),
                Pos(1, 1), Cargo(Resource.HELIUM, 5),
                Pos(1, 0), EmptyCell
        )
        val immaterial = VHashSet.of(Immaterial.ELECTROLYSIS)
        val ship = Ship(ShipType.FREIGHTER, "titanic", cells, immaterial)

        val star = Star("s", 1F, VHashMap.empty(), StarType.BROWN, VList.empty())
        assertThat(ship.canScan(star)).isFalse()
        assertThat(Ship.cellL(Pos(0, 0)).set(ship, Cargo(Resource.ENERGY_CELL, 1)).canScan(star)).isFalse()
        assertThat(Ship.cellL(Pos(0, 0)).set(ship, Cargo(Resource.ENERGY_CELL, 2)).canScan(star)).isTrue()
    }

    @Test
    fun scanned() {
        val cells = VHashMap.of(
                Pos(0, 0), Cargo(Resource.URANIUM, 3),
                Pos(0, 1), Cargo(Resource.ENERGY_CELL, 2),
                Pos(1, 1), Cargo(Resource.HELIUM, 5),
                Pos(1, 0), EmptyCell
        )
        val immaterial = VHashSet.of(Immaterial.ELECTROLYSIS)
        val ship = Ship(ShipType.FREIGHTER, "titanic", cells, immaterial)
        val star = Star("s", 1F, VHashMap.empty(), StarType.BROWN, VList.empty())

        assertThat(ship.scanned(star).cells).containsExactlyInAnyOrder(
                Tuple.of(Pos(0, 0), Cargo(Resource.URANIUM, 3)),
                Tuple.of(Pos(1, 1), Cargo(Resource.HELIUM, 5)),
                Tuple.of(Pos(1, 0), EmptyCell),
                Tuple.of(Pos(0, 1), EmptyCell)
        )
        assertThat(Ship.cellL(Pos(0, 1)).set(ship, Cargo(Resource.ENERGY_CELL, 12)).scanned(star).cells)
                .containsExactlyInAnyOrder(
                        Tuple.of(Pos(0, 0), Cargo(Resource.URANIUM, 3)),
                        Tuple.of(Pos(0, 1), Cargo(Resource.ENERGY_CELL, 10)),
                        Tuple.of(Pos(1, 1), Cargo(Resource.HELIUM, 5)),
                        Tuple.of(Pos(1, 0), EmptyCell)
                )
    }
}