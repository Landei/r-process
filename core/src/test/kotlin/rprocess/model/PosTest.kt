package rprocess.model

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class PosTest {

    @Test
    fun inRange() {
        assertThat(Pos.inRange(2, 3)).containsExactlyInAnyOrder(
                Pos(0, 0), Pos(0, 1), Pos(0, 2), Pos(0, 3),
                Pos(1, 0), Pos(1, 1), Pos(1, 2), Pos(1, 3),
                Pos(2, 0), Pos(2, 1), Pos(2, 2), Pos(2, 3)
        )
    }

    @Test
    fun compareTo() {
        assertThat(Pos(0, 0).compareTo(Pos(1, 1))).isLessThan(0)
        assertThat(Pos(1, 0).compareTo(Pos(1, 1))).isLessThan(0)
        assertThat(Pos(0, 1).compareTo(Pos(1, 0))).isLessThan(0)

        assertThat(Pos(0, 0).compareTo(Pos(0, 0))).isEqualTo(0)
        assertThat(Pos(1, 1).compareTo(Pos(1, 1))).isEqualTo(0)

        assertThat(Pos(1, 1).compareTo(Pos(0, 0))).isGreaterThan(0)
        assertThat(Pos(1, 1).compareTo(Pos(1, 0))).isGreaterThan(0)
        assertThat(Pos(1, 0).compareTo(Pos(0, 1))).isGreaterThan(0)
    }
}