package rprocess.model.galaxy

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class RouteTest {

    @Test
    fun init() {
        val c1 = Coord(2,3)
        val c2 = Coord(5,7)
        Route(c1, c2) //works
        assertThatThrownBy{ Route(c1, c1) }.hasMessage("endpoints of route must be different")
    }

    @Test
    fun dist() {
        val c1 = Coord(2,3)
        val c2 = Coord(5,7)

        val route1 = Route(c1, c2)

        assertThat(route1.dist()).isEqualTo(5)

        val c3 = Coord(0,1)
        val c4 = Coord(1,0)

        val route2 = Route(c3, c4)

        assertThat(route2.dist()).isEqualTo(2)
    }

    @Test
    fun between() {
        val c1 = Coord(2,3)
        val c2 = Coord(5,7)
        val c3 = Coord(3,4)

        val route = Route(c1, c2)

        assertThat(route.between(c1, c2)).isTrue()
        assertThat(route.between(c2, c1)).isTrue()
        assertThat(route.between(c1, c1)).isFalse()
        assertThat(route.between(c2, c2)).isFalse()
        assertThat(route.between(c1, c3)).isFalse()
        assertThat(route.between(c3, c1)).isFalse()
    }

    @Test
    fun contains() {
        val c1 = Coord(2,3)
        val c2 = Coord(5,7)
        val c3 = Coord(3,4)

        val route = Route(c1, c2)

        assertThat(route.contains(c1)).isTrue()
        assertThat(route.contains(c2)).isTrue()
        assertThat(route.contains(c3)).isFalse()
    }

    @Test
    fun crosses() {
        val c1 = Coord(2,3)
        val c2 = Coord(5,7)
        val c3 = Coord(3,6)
        val c4 = Coord(4,2)
        val c5 = Coord(0,3)

        val r1 = Route(c1, c2)
        val r2 = Route(c3, c4)
        val r3 = Route(c4, c5)
        val r4 = Route(c3, c5)

        assertThat(r1 crosses r2).isTrue()
        assertThat(r1 crosses r3).isFalse()
        assertThat(r1 crosses r4).isFalse()
        assertThat(r3 crosses r4).isTrue()
    }

    @Test
    fun connected() {
        val c1 = Coord(2,3)
        val c2 = Coord(5,7)
        val c3 = Coord(3,6)
        val c4 = Coord(4,2)

        val r1 = Route(c1, c2)
        val r2 = Route(c2, c3)
        val r3 = Route(c3, c4)

        assertThat(r1.connected(r1)).isTrue()
        assertThat(r1.connected(r2)).isTrue()
        assertThat(r1.connected(r3)).isFalse()
    }
}