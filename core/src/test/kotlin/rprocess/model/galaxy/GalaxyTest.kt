package rprocess.model.galaxy

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

import io.vavr.collection.HashMap as VHashMap
import io.vavr.collection.HashSet as VHashSet

internal class GalaxyTest {

    @Test
    fun dist_disconnected() {
        val c1 = Coord(0,0)
        val c2 = Coord(1,0)
        val c3 = Coord(1,1)
        val c4 = Coord(0,1)
        val galaxy = Galaxy(VHashMap.empty(), VHashSet.of(Route(c1, c2), Route(c3, c4)))
        assertThat(galaxy.dist(c1, c3)).isEmpty()
    }

    @Test
    fun dist_square() {
        val c1 = Coord(0,0)
        val c2 = Coord(1,0)
        val c3 = Coord(1,1)
        val c4 = Coord(0,1)
        val galaxy = Galaxy(VHashMap.empty(), VHashSet.of(Route(c1, c2), Route(c2, c3), Route(c3, c4), Route(c4, c1)))
        assertThat(galaxy.dist(c1, c1)).contains(0)
        assertThat(galaxy.dist(c1, c2)).contains(1)
        assertThat(galaxy.dist(c1, c3)).contains(2)
        assertThat(galaxy.dist(c1, c4)).contains(1)
    }

}