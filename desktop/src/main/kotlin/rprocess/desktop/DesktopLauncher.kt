package rprocess.desktop

import com.badlogic.gdx.Files
import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import org.lwjgl.input.Mouse
import rprocess.Main
import rprocess.Settings

object DesktopLauncher {

    @JvmStatic
    fun main(args: Array<String>) {
        val config = LwjglApplicationConfiguration().apply {
            title = "r-process"
            width = Settings.width
            height = Settings.height
            samples = 20
            forceExit = false
            resizable = true
            foregroundFPS = 60
            pauseWhenBackground = true
            addIcon("icon32.png", Files.FileType.Classpath)
        }
        Main.mouseInsideWindow = { Mouse.isInsideWindow() }
        LwjglApplication(Main, config)
    }
}
